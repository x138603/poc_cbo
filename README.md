# bddf_poc_cbo_mobile

## Setup SG Proxy

Add your proxy credentials in `android/gradle.properties`

Then to prevent commit this change, use this command :
```
git update-index --assume-unchanged android/gradle.properties
```

## Download Android studio and install it

https://developer.android.com/studio/

## Launch emulator
```
emulator @Nexus_6_API_26
```

## Start dev mode
```
npm run start-dev-android
```


## Linter

### Check linter
```
npm run lint
```

### Auto-fix rules
```
npm run lint-fix
```




## Show log
```
npm run log-android
```