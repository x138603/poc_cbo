module.exports = {
  ENV: 'demo',
  isPreprod: false,
  useMocks: true,
  sgSiteBaseUrl: 'http://awt.bddf.applis.bad.socgen',
};
