module.exports = {
  ENV: 'production',
  isPreprod: true,
  useMocks: false,
  sgSiteBaseUrl: 'https://m.particuliers.homologation.societegenerale.fr',
};
