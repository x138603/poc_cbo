module.exports = {
  ENV: 'dev',
  isPreprod: false,
  useMocks: true,
  sgSiteBaseUrl: 'http://awt.bddf.applis.bad.socgen',
};
