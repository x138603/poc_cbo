module.exports = {
  ENV: 'production',
  isPreprod: false,
  useMocks: false,
  sgSiteBaseUrl: 'https://m.particuliers.societegenerale.fr',
};
