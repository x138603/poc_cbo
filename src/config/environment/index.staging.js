module.exports = {
  ENV: 'staging',
  isPreprod: false,
  useMocks: false,
  sgSiteBaseUrl: 'https://appli-vf.sgmobile.fr',
};
