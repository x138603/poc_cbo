
import { black, rose, blueGrey, grey100, white, grisbold, grisregular} from './colors';
import fonts from './fonts';

const colors = {
  primary: rose,
  secondary: blueGrey,
  success: '',
  danger: '',
  warning: '',
  info: '',
  background: grey100,
  text: black,
};
const button = {
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    paddingTop:10,
    paddingBottom: 10,
    paddingLeft:30,
    paddingRight:30,
  },
  primary: {
    bg: {
        backgroundColor: colors.primary,    
    },
    label: {
        color: "#fff",
    }
  },
  primaryOutline: {
    bg: {
        backgroundColor: 'transparent',
        borderColor:colors.primary, 
        borderWidth: 1,     
    },
    label: {
        color: colors.primary,
    }
  },
  secondary: {
    bg: {
        backgroundColor: grisregular,
    },
    label: {
        color: grisbold,
    }
  },
  secondaryOutline: {
    bg: {
        backgroundColor: 'transparent',
        borderColor: grisregular, 
        borderWidth: 1,
    },
    label: {
        color: grisbold,
    }
  },
  dark: {
    bg: {
        backgroundColor: black,
    },
    label: {
        color: white,
    }
  },
  darkOutline: {
    bg: {
        backgroundColor: 'transparent',
        borderColor:black, 
        borderWidth: 1,
    },
    label: {
        color: black,
    }
  },

  light: {
    bg: {
        backgroundColor: white,
    },
    label: {
      color: grisbold,
    }
  },
  lightOutline: {
    bg: {
      backgroundColor: 'transparent',
      borderColor: white, 
      borderWidth: 1,
    },
    label: {
      color: white,
    }
  },
}

const typography = {
    h1: {
      fontFamily: "SourceSansPro-Bold",
      fontSize: 24,
      lineHeight: 28,
      color: colors.text,
    },
    h2: {
      fontFamily: "SourceSansPro-Light",
      fontSize: 24,
      lineHeight: 28,
      color: colors.text,
    },
    h3: {
      fontFamily: "SourceSansPro-Regular",
      fontSize: 20,
      lineHeight: 24,
      color: colors.text,
    },
    headline: {
      fontFamily: "SourceSansPro-SemiBold",
      fontSize: 16,
      lineHeight: 20,
      color: colors.text,
      letterSpacing: 0,
    },    
    body: {
      fontFamily: "SourceSansPro-Regular",
      fontSize: 16,
      lineHeight: 20,
      color: colors.text,
      letterSpacing: 0,
    },
    callout: {
      fontFamily: "SourceSansPro-SemiBold",
      fontSize: 14,
      lineHeight: 20,
      color: colors.text,
      letterSpacing: 0,
    },    
    subhead: {
      fontFamily: "SourceSansPro-Regular",
      fontSize: 14,
      lineHeight: 20,
      color: colors.text,
      letterSpacing: 0,
    },    
    note: {
      fontFamily: "SourceSansPro-SemiBold",
      fontSize: 12,
      lineHeight: 16,
      color: colors.text,
      letterSpacing: 0,
    },
    subnote: {
      fontFamily: "SourceSansPro-Regular",
      fontSize: 12,
      lineHeight: 16,
      color: colors.text,
      letterSpacing: 0,
    },
}

export default {
  roundness: 4,
  colors,
  typography,
  button,
  fonts,
};
