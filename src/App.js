import React from 'react';
import { StyleSheet } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './redux/store/configureStore';
import App from './navigation/routes';

import { DefaultTheme } from './styles/defaultTheme';
import { ThemeProvider } from './styles/theming';
const store = configureStore();

export default () => (
  <Provider store={store}>
    <ThemeProvider theme={DefaultTheme} >
      <App style={styles.container} />
    </ThemeProvider>
  </Provider>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
});
