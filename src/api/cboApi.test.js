import { fetchAccounts, fetchOperations, fetchOperationsFilteredByLabelAndByNbMonth } from './cboApi';


describe('cboApi', () => {
  test('fetchAccounts', () => {
    expect.assertions(1);
    return fetchAccounts(false).then((response) => {
      expect(response.data).not.toBeNull();
    });
  });

  test('fetchOperations', () => {
    expect.assertions(1);
    return fetchOperations('idTech', false).then((response) => {
      expect(response.data).not.toBeNull();
    });
  });

  test('fetchOperationsFilteredByLabelAndByNbMonth', () => {
    expect.assertions(1);
    return fetchOperationsFilteredByLabelAndByNbMonth('idTech', 'VIREMENT', 6).then((response) => {
      expect(response.data).not.toBeNull();
    });
  });
});
