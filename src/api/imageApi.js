/* eslint-disable global-require */
export const getRequiredImg = (idCategory) => {
  switch (idCategory) {
    case '100':
      return require('../assets/img/100.png');
    case '200':
      return require('../assets/img/200.png');
    case '300':
      return require('../assets/img/300.png');
    case '400':
      return require('../assets/img/400.png');
    case '500':
      return require('../assets/img/500.png');
    case '600':
      return require('../assets/img/600.png');
    case '700':
      return require('../assets/img/700.png');
    case '800':
      return require('../assets/img/800.png');
    case '900':
      return require('../assets/img/900.png');
    case '1000':
      return require('../assets/img/1000.png');
    case '1100':
      return require('../assets/img/1100.png');
    case '1200':
      return require('../assets/img/1200.png');
    case '2000':
      return require('../assets/img/2000.png');
    case '3000':
      return require('../assets/img/3000.png');
    default:
      return require('../assets/img/1200.png');
  }
};
export const getIcon = (iconId) => {
  switch (iconId) {
    case 'cartes_black':
      return {
        path:require('../assets/img/cartes_black-32x32.png'),
        size: 32,
      };
    case 'clock_black':
      return {
        path:require('../assets/img/clock_black.png'),
        size: 16,
      };
    case 'chevron_right_black':
      return {
        path:require('../assets/img/chevron_right_black.png'),
        size: 16,
      };         
    default:
      return require('../assets/img/1200.png');
  }
};
