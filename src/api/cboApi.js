/* eslint-disable no-console */
/* eslint-disable max-len */

import axios from 'axios';
import qs from 'qs';
import { sgSiteBaseUrl, useMocks } from '../config/environment';
import listePrestations from './__mocks__/liste-prestations.json';
import listePrestationsAvecMontant from './__mocks__/liste-prestations-avec-montant.json';
import listeOperations from './__mocks__/liste-operations.json';
import listeOperationsFiltree from './__mocks__/liste-operations-filtree.json';
import listeCategories from './__mocks__/liste-categories.json';
import modifierCategorie from './__mocks__/modifier-categorie-operation.json';

export const fetchAccounts = (withAmount = false) => {
  if (useMocks) { console.log(`listePrestations mock used : withAmount=${withAmount}`); return Promise.resolve({ data: withAmount ? listePrestationsAvecMontant : listePrestations }); }
  return axios.get(`${sgSiteBaseUrl}/icd/cbo/data/liste-prestations-authsec.json?n10_avecMontant=${Number(withAmount)}`);
};

export const fetchOperations = (idTechnique, supplement = false) => {
  if (useMocks) { console.log(`listeOperations mock used : idTechnique=${idTechnique}, supplement=${supplement}`); return Promise.resolve({ data: listeOperations }); }
  return axios.get(`${sgSiteBaseUrl}/icd/cbo/data/liste-operations-authsec.json?b64e200_prestationIdTechnique=${idTechnique}&an200_operationsSupplementaires=${supplement}`);
};

export const fetchOperationsFilteredByLabelAndByNbMonth = (idTechnique, libelleOperation, nbMois = 3) => {
  if (useMocks) { console.log(`listeOperationsFiltree mock used : idTechnique=${idTechnique}, libelleOperation=${libelleOperation}, nbMois=${nbMois}`); return Promise.resolve({ data: listeOperationsFiltree }); }
  const queryParams = qs.stringify({
    b64e200_prestationIdTechnique: idTechnique,
    cl200_libelleOperation: libelleOperation,
    n10_nbMois: nbMois,
  });
  return axios.get(`${sgSiteBaseUrl}/icd/cbo/data/liste-operations-authsec.json?${queryParams}`);
};

export const fetchCategories = (idSplite) => {
  if (useMocks) { console.log(`listeCategories mock used : idSplite=${idSplite}`); return Promise.resolve({ data: listeCategories }); }
  return axios.get(`${sgSiteBaseUrl}/icd/cbo/data/liste-categorie-authsec.json?an200_idSplite=${idSplite}`);
};

export const updateCategory = (data) => {
  const options = {
    method: 'POST',
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
    data: qs.stringify(data),
    url: `${sgSiteBaseUrl}/icd/cbo/data/modifier-categorie-authsec.json`,
  };
  if (useMocks) { console.log('updateCategory mock used : data=', data); return Promise.resolve({ data: modifierCategorie }); }
  return axios(options);
};
