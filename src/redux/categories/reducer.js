import {
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAILURE,
  CLOSE_CATEGORIES_MODAL,
  TOGGLE_SUB_CATEGORIES,
  SUB_CATEGORY_SELECTED,
  GET_CATEGORIES_BUDGET_RESILIE,
} from './actionTypes';

// define the initial state
const initialState = {
  isCategoriesModalVisible: false,
  categories: [],
  isCollapsed: true,
  selectedCategory: undefined,
  selectedSubCategory: undefined,
  gdbDisabled: false,
};

// define a reducer with an initalized state and logic to handle action
export default (state = initialState, action) => {
  switch (action.type) {
    case GET_CATEGORIES_SUCCESS:
      return Object.assign({}, state, {
        isCategoriesModalVisible: true,
        categories: action.categories,
        gdbDisabled: false,
      });
    case CLOSE_CATEGORIES_MODAL:
      return Object.assign({}, state, {
        isCategoriesModalVisible: false,
      });
    case TOGGLE_SUB_CATEGORIES:
      return Object.assign({}, state, {
        selectedCategory: action.selectedCategory,
      });
    case SUB_CATEGORY_SELECTED:
      return Object.assign({}, state, {
        selectedSubCategory: action.selectedSubCategory,
      });
    case GET_CATEGORIES_BUDGET_RESILIE:
      return Object.assign({}, state, {
        isCategoriesModalVisible: true,
        categories: [],
        gdbDisabled: true,
      });
    case GET_CATEGORIES_FAILURE:
    default:
      return state;
  }
};
