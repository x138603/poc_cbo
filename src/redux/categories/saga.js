import { put, takeEvery, call } from 'redux-saga/effects';
import {
  GET_CATEGORIES_REQUEST,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAILURE,
  GET_CATEGORIES_BUDGET_RESILIE,
} from './actionTypes';
import { fetchCategories } from '../../api/cboApi';

export function* getCategories(action) {
  try {
    const response = yield call(fetchCategories, action.payload.idSplite);
    const { statut, raison } = response.data.commun;

    if (statut === 'NOK' && raison === 'err_tech') {
      yield put({ type: GET_CATEGORIES_FAILURE, raison });
    } else if (statut === 'NOK' && raison === 'BUDGET_RESILIE') {
      yield put({ type: GET_CATEGORIES_BUDGET_RESILIE, error: raison });
    } else {
      yield put({ type: GET_CATEGORIES_SUCCESS, categories: response.data.donnees });
    }
  } catch (error) {
    yield put({ type: GET_CATEGORIES_FAILURE, error });
  }
}

export default function* categoriesSaga() {
  yield takeEvery(GET_CATEGORIES_REQUEST, getCategories);
}
