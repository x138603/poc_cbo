import {
  GET_CATEGORIES_REQUEST, CLOSE_CATEGORIES_MODAL, TOGGLE_SUB_CATEGORIES, SUB_CATEGORY_SELECTED,
} from './actionTypes';

export const closeCategoriesModal = () => ({ type: CLOSE_CATEGORIES_MODAL, isCategoriesModalVisible: false });

export const getCategories = idSplite => ({ type: GET_CATEGORIES_REQUEST, payload: { idSplite } });

export const toggleSubCategories = selectedCategory => ({ type: TOGGLE_SUB_CATEGORIES, selectedCategory });

export const selectSubCategory = selectedSubCategory => ({ type: SUB_CATEGORY_SELECTED, selectedSubCategory });
