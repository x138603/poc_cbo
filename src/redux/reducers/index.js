import { combineReducers } from 'redux';
import accountsReducer from '../accounts/reducer';
import operationsReducer from '../operations/reducer';
import operationsFilterBarReducer from '../../pages/CurrentOperations/components/OperationsFilterBar/reducer';
import categoriesReducer from '../categories/reducer';

export default combineReducers({
  accounts: accountsReducer,
  operations: operationsReducer,
  operationsFilterBar: operationsFilterBarReducer,
  categories: categoriesReducer,
});
