import { GET_ACCOUNTS_REQUEST, GET_ACCOUNTS_SUCCESS, GET_ACCOUNTS_FAILURE } from './actionTypes';

const initialState = {
  accountsGroupPerRole: [],
  isLoading: false,
  withAmount: false,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ACCOUNTS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case GET_ACCOUNTS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        accountsGroupPerRole: action.accountsGroupPerRole,
        withAmount: action.withAmount,
      };

    case GET_ACCOUNTS_FAILURE:
      return {
        ...state,
        isLoading: false,
        withAmount: action.withAmount,
        error: action.error,
      };

    default:
      return state;
  }
};
