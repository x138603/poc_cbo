import { GET_ACCOUNTS_REQUEST } from './actionTypes';

export const getAccounts = () => ({ type: GET_ACCOUNTS_REQUEST });
