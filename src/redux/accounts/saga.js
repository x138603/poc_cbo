import { put, takeEvery, call } from 'redux-saga/effects';
import { GET_ACCOUNTS_REQUEST, GET_ACCOUNTS_SUCCESS, GET_ACCOUNTS_FAILURE } from './actionTypes';
import { fetchAccounts } from '../../api/cboApi';

export function* getAccountsWithAmount(withAmount) {
  try {
    const response = yield call(fetchAccounts, withAmount); // fetch API
    const { statut, raison } = response.data.commun;
    if (statut === 'NOK') {
      yield put({ type: GET_ACCOUNTS_FAILURE, error: raison, withAmount });
    } else {
      const accountsGroupPerRole = response.data.donnees.syntheseParGroupeRole;
      yield put({ type: GET_ACCOUNTS_SUCCESS, accountsGroupPerRole, withAmount }); // dispatch success action
    }
  } catch (error) {
    yield put({ type: GET_ACCOUNTS_FAILURE, error, withAmount }); // dispatch error action
  }
}

/**
 * Get accounts sequentially without and with amount
 *
 * @generator
 * @yields accounts action
 */
export function* getAccounts() {
  yield call(getAccountsWithAmount, false);
  yield call(getAccountsWithAmount, true);
}

export default function* accountsSaga() {
  yield takeEvery(GET_ACCOUNTS_REQUEST, getAccounts);
}
