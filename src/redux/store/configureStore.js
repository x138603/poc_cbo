import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from '../reducers';
import rootSaga from '../sagas';

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

export default function configureStore() {
  // create a redux store with our reducer above and middleware
  const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware),
  );
    // run the saga
  sagaMiddleware.run(rootSaga);
  return store;
}
