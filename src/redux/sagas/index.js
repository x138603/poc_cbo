import { fork, all } from 'redux-saga/effects';
import accountsSaga from '../accounts/saga';
import operationsSaga from '../operations/saga';
import categoriesSaga from '../categories/saga';

export default function* rootSaga() {
  yield all([
    fork(accountsSaga),
    fork(operationsSaga),
    fork(categoriesSaga),
  ]);
}
