import { put, takeEvery, call } from 'redux-saga/effects';
import {
  GET_OPERATIONS_DETAIL_REQUEST,
  GET_OPERATIONS_DETAIL_SUCCESS,
  GET_OPERATIONS_DETAIL_FAILURE,
  GET_OPERATIONS_DETAIL_FILTERED_REQUEST,
  GET_OPERATIONS_DETAIL_FILTERED_SUCCESS,
  GET_OPERATIONS_DETAIL_FILTERED_FAILURE,
  UPDATE_CURRENT_OPERATIONS_DETAIL_REQUEST,
  UPDATE_CURRENT_OPERATIONS_DETAIL_SUCCESS,
  UPDATE_CURRENT_OPERATIONS_DETAIL_FAILURE,
  UPDATE_CATEGORY_REQUEST,
  UPDATE_CATEGORY_SUCCESS,
  UPDATE_CATEGORY_FAILURE,
} from './actionTypes';

import { fetchOperations, fetchOperationsFilteredByLabelAndByNbMonth, updateCategory } from '../../api/cboApi';

export function* getOperationsDetail(action) {
  try {
    const { idTechnique } = action.payload;
    const response = yield call(fetchOperations, idTechnique, false); // 1
    // TODO: handling bad response
    const operationsDetail = response.data.donnees;
    yield put({ type: GET_OPERATIONS_DETAIL_SUCCESS, operationsDetail }); // 2
  } catch (error) {
    yield put({ type: GET_OPERATIONS_DETAIL_FAILURE, error });
  }
}

export function* getOperationsDetailFiltered(action) {
  try {
    const { idTechnique, label, nbMonth } = action.payload;
    const response = yield call(fetchOperationsFilteredByLabelAndByNbMonth, idTechnique, label, nbMonth); // 1
    // TODO: handling bad response
    const operationsDetail = response.data.donnees;
    yield put({ type: GET_OPERATIONS_DETAIL_FILTERED_SUCCESS, operationsDetail }); // 2
  } catch (error) {
    yield put({ type: GET_OPERATIONS_DETAIL_FILTERED_FAILURE, error });
  }
}

export function* updateCurrentOperationsDetail(action) {
  try {
    const { idTechnique } = action.payload;
    const response = yield call(fetchOperations, idTechnique, true); // 1
    // TODO: handling bad response
    const operationsDetail = response.data.donnees;
    yield put({ type: UPDATE_CURRENT_OPERATIONS_DETAIL_SUCCESS, operationsDetail }); // 2
  } catch (error) {
    yield put({ type: UPDATE_CURRENT_OPERATIONS_DETAIL_FAILURE, error });
  }
}

export function* updateOperationCategory(action) {
  try {
    const data = {
      b64e200_prestationIdTechnique: action.payload.prestationIdTechnique,
      an200_id: action.payload.id,
      an200_categorisable: action.payload.categorisable,
      an200_dateOperation: action.payload.dateOperation,
      an200_idSplit: action.payload.idSplit,
      an200_idNewSousCategorie: action.payload.idNewSousCategorie,
    };
    const response = yield call(updateCategory, data);
    if (response.data.commun.statut === 'NOK') {
      yield put({ type: UPDATE_CATEGORY_FAILURE, error: response.data.commun.raison });
    }
    yield put({ type: UPDATE_CATEGORY_SUCCESS, operationUpdated: action.operationUpdated });
  } catch (error) {
    yield put({ type: UPDATE_CATEGORY_FAILURE, error });
  }
}

export default function* operationsSaga() {
  yield takeEvery(GET_OPERATIONS_DETAIL_REQUEST, getOperationsDetail);
  yield takeEvery(GET_OPERATIONS_DETAIL_FILTERED_REQUEST, getOperationsDetailFiltered);
  yield takeEvery(UPDATE_CURRENT_OPERATIONS_DETAIL_REQUEST, updateCurrentOperationsDetail);
  yield takeEvery(UPDATE_CATEGORY_REQUEST, updateOperationCategory);
}
