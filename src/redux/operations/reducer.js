import {
  GET_OPERATIONS_DETAIL_REQUEST,
  GET_OPERATIONS_DETAIL_SUCCESS,
  GET_OPERATIONS_DETAIL_FAILURE,
  UPDATE_CURRENT_OPERATIONS_DETAIL_SUCCESS,
  UPDATE_CURRENT_OPERATIONS_DETAIL_FAILURE,
  GET_OPERATIONS_DETAIL_FILTERED_REQUEST,
  GET_OPERATIONS_DETAIL_FILTERED_FAILURE,
  GET_OPERATIONS_DETAIL_FILTERED_SUCCESS,
  SELECTED_OPERATION,
  UPDATE_CATEGORY_SUCCESS,
} from './actionTypes';

const initialState = {
  operationsDetail: {
    recapitulatifCompte: {
      soldeComptable: 0,
    },
    listeOperations: [],
    listeOperationsFutures: [],
  },
  isLoading: false,
  searchResultVisible: false,
  selectedOperation: null,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    // //////////////////////GET_OPERATIONS/////////////////////////
    case GET_OPERATIONS_DETAIL_REQUEST:
      return {
        ...initialState,
        isLoading: true,
      };

    case GET_OPERATIONS_DETAIL_SUCCESS:
      return {
        ...state,
        isLoading: false,
        operationsDetail: action.operationsDetail,
      };

    case GET_OPERATIONS_DETAIL_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    // //////////////////////GET_OPERATIONS_FILTERED/////////////////////////
    case GET_OPERATIONS_DETAIL_FILTERED_REQUEST:
      return {
        isLoading: true,
        operationsDetail: {
          ...state.operationsDetail,
          listeOperations: [],
        },
      };
    case GET_OPERATIONS_DETAIL_FILTERED_SUCCESS:
      return {
        ...state,
        isLoading: false,
        searchResultVisible: true,
        operationsDetail: action.operationsDetail,
      };

    case GET_OPERATIONS_DETAIL_FILTERED_FAILURE:
      return {
        ...state,
        isLoading: false,
        searchResultVisible: false,
        error: action.error,
      };
    // //////////////////////UPDATE_CURRENT_OPERATIONS/////////////////////////
    case UPDATE_CURRENT_OPERATIONS_DETAIL_SUCCESS:
      return {
        ...state,
        operationsDetail: {
          ...state.operationsDetail,
          listeOperations: [...state.operationsDetail.listeOperations, ...action.operationsDetail.listeOperations],
        },
      };
    case UPDATE_CURRENT_OPERATIONS_DETAIL_FAILURE:
      return {
        ...state,
        error: action.error,
        operationsDetail: {
          ...state.operationsDetail,
          listeOperations: state.operationsDetail.listeOperations,
        },
      };
    // //////////////////////UPDATE_CATEGORY FOR SELECTED OPERATIONS/////////////////////////
    case SELECTED_OPERATION:
      return {
        ...state,
        isLoading: false,
        selectedOperation: action.selectedOperation,
      };
    case UPDATE_CATEGORY_SUCCESS:
      return {
        ...state,
        operationsDetail: {
          ...state.operationsDetail,
          listeOperations: [...state.operationsDetail.listeOperations.map(operationToUpdate => (
            action.operationUpdated.idOpe === operationToUpdate.idOpe ? action.operationUpdated : operationToUpdate))],
          listeOperationsFutures: [...state.operationsDetail.listeOperationsFutures.map(operationToUpdate => (
            action.operationUpdated.idOpe === operationToUpdate.idOpe ? action.operationUpdated : operationToUpdate))],
        },
      };
    default:
      return state;
  }
};
