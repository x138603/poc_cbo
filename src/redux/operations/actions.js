import {
  GET_OPERATIONS_DETAIL_REQUEST,
  GET_OPERATIONS_DETAIL_FILTERED_REQUEST,
  UPDATE_CURRENT_OPERATIONS_DETAIL_REQUEST,
  UPDATE_CATEGORY_REQUEST,
  SELECTED_OPERATION,
} from './actionTypes';

export const getOperationsDetail = idTechnique => ({ type: GET_OPERATIONS_DETAIL_REQUEST, payload: { idTechnique } });

export const getOperationsDetailFilterByLabelAndByNbMonth = (idTechnique, label, nbMonth) => ({
  type: GET_OPERATIONS_DETAIL_FILTERED_REQUEST,
  payload: { idTechnique, label, nbMonth },
});

export const updateCurrentOperationsDetail = idTechnique => ({
  type: UPDATE_CURRENT_OPERATIONS_DETAIL_REQUEST, payload: { idTechnique },
});

export const selectOperation = selectedOperation => ({ type: SELECTED_OPERATION, selectedOperation });

export const updateCategory = (prestationIdTechnique, id, categorisable, dateOperation,
  idSplit, idNewSousCategorie, operationUpdated) => ({
  type: UPDATE_CATEGORY_REQUEST,
  payload: {
    prestationIdTechnique, id, categorisable, dateOperation, idSplit, idNewSousCategorie,
  },
  operationUpdated,
});
