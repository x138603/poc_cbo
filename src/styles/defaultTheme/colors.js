import color from 'color';

export const SGred = '#e60028';
export const SGblack = '#000000';
export const white = '#ffffff';
export const rose = '#f05b6f';
export const black = '#333333';
export const grisblack = '#666666';
export const grisbold = '#7a7a7a';
export const grismedium = '#cacaca';
export const grisregular = '#ebebeb';
export const grislight = '#f4f4f4';
export const comptecourant = '#3e69c4';
export const epargne = '#36a592';
export const credits = '#a03a9c';
export const assurances = '#f5a623';
export const ardoise = '#3b4357';
export const validation = '#3ac596';
export const information = '#4ebaca';
export const avertissement = '#e74c3c';

export const black60 = color("#000000")
                        .alpha(0.6)
                        .rgb()
                        .string();

export const black40 = color("#000000")
                        .alpha(0.4)
                        .rgb()
                        .string();

export const black20 = color("#000000")
                        .alpha(0.2)
                        .rgb()
                        .string();       
export const black10 = color("#000000")
                        .alpha(0.1)
                        .rgb()
                        .string();

export const black05 = color("#000000")
                        .alpha(0.05)
                        .rgb()
                        .string();




export const keppel = '#36a592'; // saturated light cold bluish cyan
export const coldGreen = '#60b747'; // saturated light cold green
export const coldCyan = '#b0dcd4'; // very unsaturated very light cold cyan

export const indigo = '#3e69c4'; // saturated very light cold azure
export const blueGrey = '#ebebeb';

export const red240 = '#f05b6f'; // saturated very light warm red
export const orange = '#f5a623'; // very saturated very light warm orange

export const purple = '#a03a9c'; 


export const grey50 = '#fafafa';
export const grey100 = '#f5f5f5';
export const grey200 = '#eeeeee';
export const grey300 = '#e0e0e0';
export const grey400 = '#bdbdbd';
export const grey500 = '#9e9e9e';
export const grey600 = '#757575';
export const grey700 = '#616161';
export const grey800 = '#424242';
export const grey900 = '#212121';