import { createTheming } from '@callstack/react-theme-provider';
import DefaultTheme from './defaultTheme';

export const {
    ThemeProvider,
    withTheme,
} =  createTheming(DefaultTheme);