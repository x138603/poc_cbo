import React from 'react';
import {
  View, Text, StyleSheet, SectionList,
} from 'react-native';
import _ from 'lodash';
import { formatDateFr } from '../../utils/dateUtils';
import {Cellule} from '../../components/';

export default (props) => {
  const { screenProps } = props;
  const { operationsDetail: { listeOperationsFutures } } = screenProps;
  const ListFuturOperationsBroupedByDateEche = (listeOperationsFutures && listeOperationsFutures.length > 0)
    ? _.groupBy(listeOperationsFutures, 'dateEcheance')
    : [];
  const mappedListOperations = Object.keys(ListFuturOperationsBroupedByDateEche).map(key => ({
    title: formatDateFr(key),
    data: ListFuturOperationsBroupedByDateEche[key],
  }));

  if (listeOperationsFutures && listeOperationsFutures.length > 0) {
    return (
      <View>
        <SectionList
          sections={mappedListOperations}
          renderItem={({ item }) => (
            <Cellule 
                  icon="clock_black"
                  amount={item.montant.value}
                  label={item.libelleAAfficher}
              ></Cellule>
          )
          }
          renderSectionHeader={({ section }) => <Text style={styles.sectionHeader}>{section.title}</Text>}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
  return (<Text style={styles.textStyle}>{'Vous n\'avez aucune operation à venir sur ce compte'}</Text>);
};

const styles = StyleSheet.create({
  sectionHeader: {
    paddingTop: 8,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: '#e6e6e6',
  },
  textStyle: {
    padding: 8,
    textAlign: 'center',
  },
});
