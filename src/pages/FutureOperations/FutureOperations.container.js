import { connect } from 'react-redux';
import { updateCurrentOperationsDetail, selectOperation } from '../../redux/operations/actions';
import { getCategories } from '../../redux/categories/actions';
import FutureOperations from './FutureOperations.component';

const mapStateToProps = state => ({
  operationsDetail: state.operations.operationsDetail,
});

const mapDispatchToProps = {
  updateCurrentOperationsDetail,
  getCategories,
  selectOperation,
};

export default connect(mapStateToProps, mapDispatchToProps)(FutureOperations);
