import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { SgButton, Title } from '../../components';

export default class Login extends Component {
    static navigationOptions = {
        header: null
    };
    render() {
        return (
            <View style={styles.MainContainer} >
                <View >
                    <Image
                        style={styles.image}
                        source={require('../../assets/img/logo.png')}
                        resizeMode="contain"
                    />
                </View>
                <View style={{flex:4, justifyContent: 'flex-end', marginBottom:60 }} >
                    <Title h2>Bonjour Marie!</Title>
                </View>
                <View  style={{flex:3 }}>
                    <SgButton
                        title="Connexion"
                        role="primary"
                        onPress={() => this.props.navigation.navigate('SynthesisTabView')}
                    />
                </View>
                <View style={styles.ButtonsSecondaryContainerStyle} >
                    <View style={{flex:1, padding: 8}}>
                        <SgButton
                            title="Urgences"
                            role="secondary"
                            outline={true}
                            onPress={() => this.props.navigation.navigate('Home')}
                        />
                    </View>
                    <View style={{flex:1, padding: 8}}>
                        <SgButton
                            title="Contact"
                            role="secondary"
                            outline={true}
                            onPress={() => this.props.navigation.navigate('Contact')}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white',
        justifyContent: 'space-between',
    },
    image: {
        margin: 32,
        height: 70,
        width: 350,
    },
    ButtonsSecondaryContainerStyle: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        marginBottom: 24,
    },
})