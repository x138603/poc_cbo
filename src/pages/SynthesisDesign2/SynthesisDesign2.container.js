import { connect } from 'react-redux';
import { getAccounts } from '../../redux/accounts/actions';
import SynthesisDesign2 from './SynthesisDesign2.component';

const mapStateToProps = state => ({
  accountsGroupPerRole: state.accounts.accountsGroupPerRole,
  withAccountsAmount: state.accounts.withAmount,
});

const mapDispatchToProps = {
  getAccounts,
};

export default connect(mapStateToProps, mapDispatchToProps)(SynthesisDesign2);
