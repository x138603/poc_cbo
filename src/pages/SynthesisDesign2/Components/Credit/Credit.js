import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { AddAccountCard } from '../../../../components/AddAccountCard';
import { SAVING } from '../../constants';

export default class Credit extends Component {
  cardConf = () => ({
    pictoType: SAVING,
    textConf: {
      title: {
        value: "Toutes nos solutions crédits",
        show: true
      },
      subTitle: {
        value: "Découvrez nos solutions",
        show: false
      }
    },
    buttonConf: {
      symbole: "+",
      onHandleClick: () => console.log("Saving button add clicked ")
    },
  });

  render() {
    return (
      <View style={{ flexDirection: 'column', alignItems: 'flex-start', padding: 8 }}>
        <Text style={{ color: "black", fontSize: 20 }}> Crédit </Text>
        <AddAccountCard {...this.cardConf()} />
      </View>
    )
  }
}