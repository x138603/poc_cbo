
import { SAVING, COMPTE_EPARGNE } from '../../constants';
import AbstractAccounts from '../AbstractAccounts';

export default class Saving extends AbstractAccounts {

  getCurrentAccountType() {
    return COMPTE_EPARGNE;
  }

  getAccountConf() {
    const { accountsGroupPerRole, withAccountsAmount } = this.props;

    return {
      accountsGroupPerRole,
      withAccountsAmount,
      amountStyle: { color: "#36a592", fontSize: 18 },
      cardConf: {
        pictoType: SAVING,
        textConf: {
          title: {
            value: "Epargnez en toute tranquilité",
            show: true
          },
          subTitle: {
            value: "Découvrez nos solutions",
            show: true
          }
        },
        buttonConf: {
          symbole: ">",
          onHandleClick: () => console.log("Saving button add clicked ")
        },
      }
    };
  }
}

