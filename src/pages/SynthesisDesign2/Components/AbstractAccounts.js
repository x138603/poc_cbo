import React, { Component } from 'react';
import { Text, View, StyleSheet, FlatList, Dimensions } from 'react-native';
import { AddAccountCard } from '../../../components/AddAccountCard';
import { ACCOUNT_COLOR } from '../constants';
import _ from 'lodash';
import Card from '../../../components/Card';
import { ACCOUNT_TYPE } from '../../SynthesisTabView/constants';
export default class AbstractAccounts extends Component {

  renderCurrentAccountList(groupedAccount, cardConf) {
    const { withAccountsAmount } = this.props;
    const customAddAcciuntStyle = {height: 80};
    const screenWidth = Dimensions.get('window').width;
    groupedAccount.push({addAccount: true})
    return (
      <FlatList
        data={groupedAccount}
        horizontal={true}
        renderItem={({ item, index }) => {
          if (index === groupedAccount.length - 1) {
            return (
              <View style={{width: 360}}>
                <AddAccountCard {...cardConf} customAddAcciuntStyle={customAddAcciuntStyle} />
              </View>
            )
          }
          return (<Card
            theme={ACCOUNT_COLOR[item.typePrestation]}
            item={item}
            press={() => { this.showDetailAccount(item, ACCOUNT_COLOR[item.typePrestation]); }}
            withAccountsAmount={withAccountsAmount}
            customDesign={{width: screenWidth - (0.12*screenWidth)}}
          />)
        }
        }
      />
    );
  }

  showDetailAccount(item, theme) {
    const { navigation } = this.props;
    navigation.navigate('SynthesisDetails', { item, theme });
  }

  getCurrentAccountType() {
    return null;
  }

  getAccountConf() {
    return null;
  }

  render() {
    const { amountStyle, cardConf, accountsGroupPerRole, withAccountsAmount } = this.getAccountConf();
    const groupedAccount = (accountsGroupPerRole[0].prestations && accountsGroupPerRole[0].prestations.length > 0)
      ? _.groupBy(accountsGroupPerRole[0].prestations, 'typePrestation') : [];
    const soldeGroupe = _.find(accountsGroupPerRole[0].soldesParGroupe, { groupeProduit: this.getCurrentAccountType() }) || { solde: 0 };

    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 8 }}>
          <Text style={{ color: "black", fontSize: 16 }}> {ACCOUNT_TYPE[this.getCurrentAccountType()]} </Text>
          <Text style={amountStyle}> {withAccountsAmount ? `${soldeGroupe.solde} €` : 'En cours de traitement'}</Text>
        </View>
        <View >
          {
            groupedAccount[this.getCurrentAccountType()] && groupedAccount[this.getCurrentAccountType()].length > 0
              ?
              this.renderCurrentAccountList(groupedAccount[this.getCurrentAccountType()], cardConf)
              : null
          }
        </View>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    maxHeight: "68%",
  },
});
