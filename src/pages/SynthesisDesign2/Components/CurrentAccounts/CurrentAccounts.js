import { COMPTE_A_VUE } from '../../constants';
import AbstractAccounts from '../AbstractAccounts';

export default class CurrentAccounts extends AbstractAccounts {

  getCurrentAccountType() {
    return COMPTE_A_VUE;
  }

  getAccountConf() {
    const {accountsGroupPerRole, withAccountsAmount  } = this.props;
    return {
      accountsGroupPerRole,
      withAccountsAmount,
      amountStyle: { color: "#3e69c4", fontSize: 18 },
      cardConf: {
        textConf: {
          title: {
            value: "Ajoutez un compte",
            show: true
          },
          subTitle: {
            value: "here is a subtitle",
            show: false
          }
        },
        buttonConf: {
          symbole: "+",
          onHandleClick: () => console.log("CurrentAccounts button add clicked ")
        },
      }
    };
  }
}

