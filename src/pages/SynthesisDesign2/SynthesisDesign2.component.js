
import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, YellowBox } from 'react-native';
import appStyle from '../../appStyle';
import { dateNow } from '../../utils/dateUtils';
import Credit from './Components/Credit/Credit';
import Insurance from './Components/Insurance/Insurance';
import Saving from './Components/Saving/Saving';
import CurrentAccounts from './Components/CurrentAccounts/CurrentAccounts';

export class SynthesisDesign2 extends PureComponent {
  componentDidMount() {
    const { getAccounts } = this.props;
    YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
    // get accounts (2 calls without and with amount)
    getAccounts();
  }

  render() {
    const { accountsGroupPerRole, withAccountsAmount, navigation } = this.props;

    if (accountsGroupPerRole.length > 0) {
      return (
        <View style={styles.main}>
          <View style={styles.totalAmount}>
            <Text style={{ color: "black", fontSize: 24 }}> {accountsGroupPerRole[0].montantTotal} € </Text>
            <Text style={{ color: "#838383", fontSize: 13 }}> Solde total au {dateNow()} </Text>
          </View>
          <View style={styles.containerViews}>
            <CurrentAccounts navigation={navigation} accountsGroupPerRole={accountsGroupPerRole} withAccountsAmount={withAccountsAmount} />
            <Saving navigation={navigation} accountsGroupPerRole={accountsGroupPerRole} withAccountsAmount={withAccountsAmount} />
            <Insurance navigation={navigation} accountsGroupPerRole={accountsGroupPerRole} withAccountsAmount={withAccountsAmount} />
            <Credit />
          </View>
        </View>
      )
    }
    return (
      <ActivityIndicator style={styles.loaderContainer} color={appStyle.colors.corail} size="large" />
    );

  }
}

export default SynthesisDesign2;

const styles = StyleSheet.create({
  main: { flex: 1, flexDirection: 'column' },
  totalAmount: { flexDirection: 'column', alignItems: 'center', padding: 8 },
  containerViews: { height: '86%' },
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
