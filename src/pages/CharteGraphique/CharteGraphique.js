import React, { Component } from 'react';
import color from 'color'
import {StyleSheet, View, ScrollView, FlatList, Text, TouchableHighlight} from 'react-native';
import elements  from './elements'

export default class CharteGraphique extends Component {
    constructor(props) {
        super(props);
    }
    renderSeparator = () => (
        <View
          style={styles.divider}
        />
      );

    navigate(item) {
        const { navigation } = this.props;
        navigation.navigate(item.title)
    }
    renderItem (item ) {
        
        return (
            <TouchableHighlight onPress={this.navigate.bind(this, item)}>
                <Text style={styles.item}>{item.title}</Text>
            </TouchableHighlight>
        );
    }
    render() {
        const data =  Object.keys(elements).map(id => ({ keys:id, title: id }));
        return (
            <ScrollView style={styles.container}>
                <FlatList
                    ItemSeparatorComponent={this.renderSeparator}
                    data={data}
                    renderItem={({ item }) =>  this.renderItem(item)}
                    keyExtractor={item => item.keys}
                />
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    item: {
        color: color("#000000")
            .alpha(0.87)
            .rgb()
            .string(),
        padding: 10,
        fontSize: 16,
    },
    divider: {
        backgroundColor: color("#000000")
            .alpha(0.12)
            .rgb()
            .string(),
        height: 1,
    }
  })