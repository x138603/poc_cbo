import React, { Component } from 'react';
import {View, StyleSheet} from 'react-native';

import {CardInfo} from '../../components/';
import {withTheme} from '../../styles/theming';

class CardInfoExample extends Component {
    render() {
        return (
            <View style={styles.container}>
                <CardInfo
                    style={{marginBottom:8}}
                    type="comptecourant"
                    number="02500 00050013250"
                    name="Frédérique Demonceau"
                />
                <CardInfo
                    style={{marginBottom:8}}
                    type="epargne"
                    number="02500 00050013250"
                    name="Frédérique Demonceau"
                />
                <CardInfo
                    style={{marginBottom:8}}
                    type="credits"
                    number="02500 00050013250"
                    name="Frédérique Demonceau"
                /> 
                <CardInfo
                    style={{marginBottom:8}}
                    type="assurances"
                    number="02500 00050013250"
                    name="Frédérique Demonceau"
                /> 
            </View>
        );
    }
}
export default withTheme(CardInfoExample);

const styles = StyleSheet.create({
    container: {
      margin:8,
    }
});