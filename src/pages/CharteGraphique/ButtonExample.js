import React, { Component } from 'react';
import {ScrollView, View, StyleSheet} from 'react-native';

import { SgButton } from '../../components/';
import { withTheme } from '../../styles/theming';
class ButtonExample extends Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <SgButton
                  style={styles.button}
                    title="Primary"
                    role="primary"
                    onPress={() => {}}
                />
                <SgButton
                  style={styles.button}
                    title="Primary outline"
                    role="primary"
                    outline={true}
                    onPress={() => {}}
                />
                <SgButton
                  style={styles.button}
                    title="Secondary"
                    role="secondary"
                    onPress={() => {}}
                />
                <SgButton
                  style={styles.button}
                    title="Secondary outline"
                    role="secondary"
                    outline={true}
                    onPress={() => {}}
                />
                <SgButton
                  style={styles.button}
                    title="Dark"
                    role="dark"
                    onPress={() => {}}
                />
                <SgButton
                  style={styles.button}
                    title="Dark outline"
                    role="dark"
                    outline={true}
                    onPress={() => {}}
                />
                <View style={styles.darkBg}>
                  <SgButton 
                    style={styles.button}
                      title="Light"
                      role="light"
                      onPress={() => {}}
                  />
                  <SgButton
                      style={styles.button}
                      title="Light Outline"
                      role="light"
                      outline={true}
                      onPress={() => {}}
                  />
                </View>
                <SgButton
                  style={styles.button}
                    role="primary"
                    loading={true}
                    onPress={() => {}}
                />
                <SgButton
                  style={styles.button}
                    role="primary"
                    loading={true}
                    outline={true}
                    onPress={() => {}}
                />
            </ScrollView>      
        );
    }
}
const styles = StyleSheet.create({
    container: {
      backgroundColor: "#ffffff",
      padding: 16,
      paddingBottom:30,
      flex: 1,
    },
    darkBg: {
      padding:20,
      marginBottom: 15, 
      backgroundColor: "#4ebaca",
    },
    button: {
      marginBottom: 15,      
      width: 250,
    },
  });
export default withTheme(ButtonExample);