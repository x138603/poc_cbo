import React, { Component } from 'react';
import {View, StyleSheet} from 'react-native';
import { withTheme } from '../../styles/theming';
import {Card} from '../../components/';
import { comptecourant, epargne, assurances } from '../../styles/defaultTheme/colors';
class CardExample extends Component {
    
    render() {
        const item = {
                labelToDisplay:"Compte Bancaire",
                cardNumber:"cardNumber",
                amout:"15",
                comingCaption:"comingCaption",
                provisionalCaption:"provisionalCaption",
                typePrestation:"COMPTE_A_VUE",
                numeroCompteFormate: "4979 3298 0761 0987",
                soldes:{soldeTotal:"150", soldeEnCours: "60"}
            };
        const item2 = {
                labelToDisplay:"Compte Bancaire",
                cardNumber:"cardNumber",
                amout:"15",
                comingCaption:"comingCaption",
                provisionalCaption:"provisionalCaption",
                typePrestation:"TITULAIRE",
                numeroCompteFormate: "4979 3298 0761 0987",
                soldes:{soldeTotal:"150", soldeEnCours: "60"}
            };
        const item3 = {
                labelToDisplay:"Compte Bancaire",
                cardNumber:"cardNumber",
                amout:"15",
                comingCaption:"comingCaption",
                provisionalCaption:"provisionalCaption",
                typePrestation:"TITULAIRE",
                numeroCompteFormate: "4979 3298 0761 0987",
                soldes:{soldeTotal:"150", soldeEnCours: "60"}
            };

        return (
            <View style={styles.container}>
                <Card
                    theme={"comptecourant"}
                    item={item}
                    press={() => { }}
                    withAccountsAmount={true}
                />
                <Card
                    theme={"epargne"}
                    item={item2}
                    press={() => { }}
                    customDesign={{width: 310}}
                />
                <Card
                    theme={"assurances"}
                    item={item3}
                    press={() => { }}
                    customDesign={{width: 260}}
                />
            </View>      
        );
    }
}
const styles = StyleSheet.create({
    container: {
      padding: 16,
      flex: 1,
    },
    text: {
      marginVertical: 4,
    },
  });
export default withTheme(CardExample);