
import TypographyExample from './TypographyExample';
import ButtonExample from './ButtonExample';
import CardExample from './CardExample';
import CellulesExample from './CellulesExample';
import CardInfoExample from './CardInfoExample';
import ProgressBarExample from './ProgressBarExample';

export default elements = {
    typography: TypographyExample,
    button: ButtonExample,
    card: CardExample,
    cellules: CellulesExample,
    cardInfo: CardInfoExample,
    progressBar: ProgressBarExample
};