import React, { Component } from 'react';
import {View, StyleSheet} from 'react-native';

import {
    Note,
    Subnote,
    Subhead,
    Title,
    Headline,
    Body,
    Callout,
} from '../../components/';
import { withTheme } from '../../styles/theming';
class TypographyExample extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Title h1 style={styles.text}>24/H1/Black_33/bold/theme_color</Title>
                <Title h2 style={styles.text}>24/H2/Black_33/light/theme_color</Title>
                <Title h3 style={[styles.text, styles.customColor]}>20/H3/Black_33/regular/custom_color</Title>
                <Headline style={styles.text}>16/HEADLINE/Black_33/semibold</Headline>
                <Body style={styles.text}>16/BODY/Black_33/regular</Body>
                <Callout style={styles.text}>14/CALLOUT/Black_33/semibold</Callout>
                <Subhead style={styles.text}>14/SUBHEAD/Black_33/regular</Subhead>
                <Note style={styles.text}>12/NOTE/Black_33/semibold</Note>
                <Subnote style={styles.text}>12/SUBNOTE/Black_33/regular</Subnote>
            </View>      
        );
    }
}
const styles = StyleSheet.create({
    container: {
      backgroundColor: "#ffffff",
      padding: 16,
      flex: 1,
    },
    text: {
      marginVertical: 10,
    },
    customColor: {
        color: "#f05b6f"
    }
  });
export default withTheme(TypographyExample);