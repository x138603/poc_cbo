import React, { Component } from 'react';
import {View, StyleSheet} from 'react-native';

import {ProgressBar} from '../../components/';
import { withTheme } from '../../styles/theming';

class ProgressBarExample extends Component {
    state = {
        progress: 0,
    };

    componentDidMount() {
        this._interval = setInterval(
            () =>
                this.setState(state => ({
                progress: state.progress < 1 ? state.progress + 0.01 : 0,
                })),
            16
            );
        }
    componentWillUnmount() {
        clearInterval(this._interval);
    }
    render() {
        const theme = this.props.theme;
        return (                    
            <View style={{margin:4}}>                
                <ProgressBar progress={this.state.progress} />
                <ProgressBar color={theme.colors.epargne} progress={0.7} />
            </View>           
        );
    }
}
export default withTheme(ProgressBarExample);