import React, { Component } from 'react';
import {View, StyleSheet} from 'react-native';

import {
    Cellule
} from '../../components/';
import { withTheme } from '../../styles/theming';
class CellulesExample extends Component {
    render() {
        return (                    
            <View style={{margin:4}}>                
                <Cellule 
                    icon="cartes_black"
                    amount="300"
                    label="Virement compte à compte"
                ></Cellule>
                <Cellule 
                    icon="cartes_black"
                    amount="2500"
                    label="Virement compte à compte"
                    caption="Virement compte à compte"
                ></Cellule>

                <Cellule 
                    icon="cartes_black"
                    amount="120"
                    label="Virement compte à compte Motif: vacances"
                    caption="Virement compte à compte"
                ></Cellule>

                <Cellule 
                    icon="clock_black"
                    amount="-200"
                    caption="Virement compte à compte"
                    chevronRight={true}
                ></Cellule> 
            </View>           
        );
    }
}
export default withTheme(CellulesExample);