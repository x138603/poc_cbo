import { connect } from 'react-redux';
import {
  getOperationsDetail,
  getOperationsDetailFilterByLabelAndByNbMonth,
} from '../../../../redux/operations/actions';
import { updateNbMonth, updateSearchTerm } from './actions';
import OperationsFilterBar from './OperationsFilterBar.component';

const mapStateToProps = state => ({
  operationsDetail: state.operations.operationsDetail,
  searchResultVisible: state.operations.searchResultVisible,
  searchTerm: state.operationsFilterBar.searchTerm,
  nbMonth: state.operationsFilterBar.nbMonth,
});

const mapDispatchToProps = {
  getOperationsDetail,
  getOperationsDetailFilterByLabelAndByNbMonth,
  updateNbMonth,
  updateSearchTerm,
};

export default connect(mapStateToProps, mapDispatchToProps)(OperationsFilterBar);
