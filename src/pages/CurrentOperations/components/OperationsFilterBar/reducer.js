import {
  UPDATE_NB_MONTH,
  UPDATE_SEARCH_TERM,
  RESET_FILTER_BAR,
} from './actionTypes';

const initialState = {
  searchTerm: '',
  nbMonth: 3,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_SEARCH_TERM:
      return {
        ...state,
        searchTerm: action.searchTerm,
      };
    case UPDATE_NB_MONTH:
      return {
        ...state,
        nbMonth: action.nbMonth,
      };
    case RESET_FILTER_BAR:
      return initialState;
    default:
      return state;
  }
};
