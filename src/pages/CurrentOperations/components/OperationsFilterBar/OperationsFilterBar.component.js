import React, { PureComponent } from 'react';
import {
  View, Text, TextInput, Picker, TouchableOpacity, Image, StyleSheet,
} from 'react-native';
import Amount from '../../../../components/Amount';
import * as searchFailedImg from '../../../../assets/img/search-failed.png';
import * as searchWhiteImg from '../../../../assets/img/search-white.png';

export default class OperationsFilterBar extends PureComponent {
  renderSearchResults() {
    const { searchResultVisible } = this.props;
    if (searchResultVisible) {
      const { operationsDetail: { listeOperations } } = this.props;
      if (listeOperations && listeOperations.length > 0) {
        return (
          <View style={styles.searchResultSuccessContainer}>
            <Text style={styles.searchResultSuccessMsg}>{listeOperations.length} résultat(s) pour votre recherche</Text>
            <Amount style={styles.searchResultSuccessAmount}>{listeOperations.reduce((a, b) => a + b.mnt, 0)}</Amount>
          </View>
        );
      }
      return (
        <View style={styles.searchResultFailedContainer}>
          <Text style={{ fontWeight: 'bold' }}>
            {'Nous sommes désolés, il n\'y a aucun résultat correspondant à votre recherche.'}
          </Text>
          <Text>{'Veuillez vérifier l\'orthographe des mots saisis ou complétez les par un nouveau mot clé.'}</Text>
          <Image source={searchFailedImg} />
        </View>
      );
    }
    return null;
  }

  render() {
    const {
      updateSearchTerm,
      searchTerm,
      updateNbMonth,
      nbMonth, prestationIdTechnique,
      getOperationsDetail,
      getOperationsDetailFilterByLabelAndByNbMonth,
    } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.barContainer}>
          <TextInput
            style={styles.searchInput}
            placeholder="Rechercher"
            onChangeText={text => updateSearchTerm(text)}
            value={searchTerm}
          />
          <Picker
            style={styles.nbMonthPicker}
            selectedValue={nbMonth}
            onValueChange={itemValue => updateNbMonth(itemValue)}
          >
            <Picker.Item label="Sur les 3 derniers mois" value={3} />
            <Picker.Item label="Sur les 6 derniers mois" value={6} />
            <Picker.Item label="Sur les 12 derniers mois" value={12} />
            <Picker.Item label="Sur les 24 derniers mois" value={24} />
          </Picker>
          <TouchableOpacity
            style={styles.searchBtnContainer}
            onPress={() => {
              if (searchTerm.trim() === '') {
                getOperationsDetail(prestationIdTechnique);
              } else { // has search term
                getOperationsDetailFilterByLabelAndByNbMonth(prestationIdTechnique, searchTerm, nbMonth);
              }
            }}
          >
            <Image style={styles.searchBtnImage} source={searchWhiteImg} />
          </TouchableOpacity>
        </View>
        {this.renderSearchResults()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 5,
    marginRight: 5,
  },
  barContainer: {
    flexDirection: 'row',
    marginBottom: 5,
    backgroundColor: 'white',
    borderRadius: 5,
  },
  searchInput: {
    flex: 2,
  },
  nbMonthPicker: {
    flex: 2,
  },
  searchBtnContainer: {
    backgroundColor: '#666',
    width: 50,
    height: 50,
    padding: 15,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
  searchBtnImage: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: Image.resizeMode.contain,
  },
  searchResultSuccessContainer: {
    flex: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  searchResultSuccessMsg: {
    marginRight: 2,
  },
  searchResultSuccessAmount: {
    marginLeft: 2,
  },
  searchResultFailedContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginLeft: 15,
    marginRight: 15,
  },
});
