import { UPDATE_NB_MONTH, UPDATE_SEARCH_TERM, RESET_FILTER_BAR } from './actionTypes';

export const updateSearchTerm = searchTerm => ({ type: UPDATE_SEARCH_TERM, searchTerm });

export const updateNbMonth = nbMonth => ({ type: UPDATE_NB_MONTH, nbMonth });

export const resetFilterBar = () => ({ type: RESET_FILTER_BAR });
