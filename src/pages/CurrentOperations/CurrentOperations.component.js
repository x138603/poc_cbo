import React, { Component } from 'react';
import {
  View, Text, SectionList, StyleSheet, ActivityIndicator,
} from 'react-native';
import _ from 'lodash';
import {Cellule, Subhead} from '../../components/'
import CardOperation from '../../components/CardOperation';
import OperationsFilterBar from './components/OperationsFilterBar';
import { formatDateFr } from '../../utils/dateUtils';
import appStyle from '../../appStyle';

export default class CurrentOperations extends Component {
  componentDidMount() {
    const { resetFilterBar } = this.props;
    resetFilterBar();
  }

  render() {
    const {
      updateCurrentOperationsDetail,
      searchResultVisible,
      isLoading,
      screenProps: { item, operationsDetail: { listeOperations } },
    } = this.props;
    const ListOperationsGroupedByDateOpe = (listeOperations && listeOperations.length > 0)
      ? _.groupBy(listeOperations, 'dateOpe')
      : [];
    const mappedListOperations = Object.keys(ListOperationsGroupedByDateOpe).map(key => ({
      title: formatDateFr(key),
      data: ListOperationsGroupedByDateOpe[key],
    }));

    if (listeOperations && (listeOperations.length > 0 || searchResultVisible)) {
      const { getCategories, selectOperation } = this.props;
      return (
        <View style={styles.container}>
          <SectionList
            sections={mappedListOperations}
            /* eslint-disable-next-line no-shadow */
            renderItem={({ item }) => (
              <Cellule 
                  icon="cartes_black"
                  amount={item.mnt}
                  label={item.libOpe}
              ></Cellule>
            )}
            renderSectionHeader={({ section }) => <Subhead style={styles.sectionHeader}>{section.title}</Subhead>}
            /* eslint-disable-next-line no-shadow */
            keyExtractor={(item, index) => index}
            onEndReachedThreshold={0.5}
            onEndReached={() => {
              if (!searchResultVisible) {
                updateCurrentOperationsDetail(item.idTechnique, true);
              }
            }}
          />
        </View>
      );
    }
    if (isLoading) {
      return (
        <ActivityIndicator style={styles.loaderContainer} color={appStyle.colors.corail} size="large" />
      );
    }
    return (<Text style={styles.textStyle}>{'Vous n\'avez aucune operation sur ce compte'}</Text>);
  }
}

const styles = StyleSheet.create({
  container: {    
    backgroundColor: 'white',
  },
  sectionHeader: {
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
  },
  textStyle: {
    padding: 8,
    textAlign: 'center',
  },
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
