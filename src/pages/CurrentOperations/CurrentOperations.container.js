import { connect } from 'react-redux';
import { updateCurrentOperationsDetail, selectOperation } from '../../redux/operations/actions';
import { getCategories } from '../../redux/categories/actions';
import { resetFilterBar } from './components/OperationsFilterBar/actions';
import CurrentOperations from './CurrentOperations.component';

const mapStateToProps = state => ({
  operationsDetail: state.operations.operationsDetail,
  searchResultVisible: state.operations.searchResultVisible,
  isLoading: state.operations.isLoading,
});

const mapDispatchToProps = {
  updateCurrentOperationsDetail,
  getCategories,
  selectOperation,
  resetFilterBar,
};

export default connect(mapStateToProps, mapDispatchToProps)(CurrentOperations);
