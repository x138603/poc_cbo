import React, { Component } from 'react';
import {
  FlatList, StyleSheet, Text, View, TouchableOpacity,
} from 'react-native';
import Category from './Category';

export default class Categories extends Component {
  renderCategories = ({ item }) => {
    const { toggleSubCategories } = this.props;
    return (
      <View>
        <TouchableOpacity onPress={() => { toggleSubCategories(item); }}>
          <Category category={item} />
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    const { categories, gdbDisabled } = this.props;
    if (gdbDisabled) {
      return (
        <Text style={styles.item}>
            Afin de pouvoir modifier les catégories, vous devez activer le service gestion de Budget
        </Text>
      );
    } if (categories && categories.length > 0) {
      return (
        <FlatList
          styles={styles.container}
          data={categories}
          extraData={this.props}
          renderItem={this.renderCategories}
          keyExtractor={(item, index) => `${index}`}
        />
      );
    }
    return (<Text style={styles.item}>Chargement...</Text>);
  }
}

const styles = StyleSheet.create({
  sectionHeaderTitle: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: '#e6e6e6',
  },
  sectionHeaderAmount: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold',
    backgroundColor: '#e6e6e6',
  },
  groupLabel: {
    color: 'white',
    fontSize: 16,
    paddingTop: 4,
    paddingBottom: 4,
    marginBottom: 4,
    paddingLeft: 10,
    fontWeight: 'bold',
    backgroundColor: '#4285f4',
  },
});
