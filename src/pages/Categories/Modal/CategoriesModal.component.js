import React, { Component } from 'react';
import {
  Modal, Text, TouchableHighlight, View,
} from 'react-native';
import Categories from '../Categories.container';

export default class CategoriesModal extends Component {
  updateAndClose() {
    const {
      selectedOperation, selectedCategory, selectedSubCategory, updateCategory, closeCategoriesModal,
    } = this.props;
    if (selectedSubCategory) {
      const idTech = selectedOperation.idOpe.split('/')[2];
      const id = selectedOperation.idOpe.split('/')[0];
      const categorisable = `${selectedOperation.categorisable}`;
      const dateOperation = selectedOperation.idOpe.split('/')[1];
      const idSplit = selectedOperation.cat.id === '2000' ? 1 : 0;
      const idNewSousCategorie = selectedSubCategory.id;

      const operationToUpdate = Object.assign({}, selectedOperation, {
        cat: {
          id: selectedCategory.id,
          lib: selectedCategory.libelle,
        },
        ssCat: {
          id: selectedSubCategory.id,
          lib: selectedSubCategory.libelle,
        },
      });
      updateCategory(idTech, id, categorisable, dateOperation, idSplit, idNewSousCategorie, operationToUpdate);
    }
    closeCategoriesModal();
  }

  render() {
    const { isCategoriesModalVisible, closeCategoriesModal } = this.props;
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={isCategoriesModalVisible}
          onRequestClose={() => {
            closeCategoriesModal();
          }}
        >

          <View>
            <TouchableHighlight
              onPress={() => {
                closeCategoriesModal();
              }}
            >
              <Text>Annuler</Text>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={() => {
                this.updateAndClose();
              }}
            >
              <Text>Valider</Text>
            </TouchableHighlight>
          </View>
          <Categories />
        </Modal>
      </View>
    );
  }
}
