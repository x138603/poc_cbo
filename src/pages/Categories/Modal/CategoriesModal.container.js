import { connect } from 'react-redux';
import CategoriesModal from './CategoriesModal.component';
import { selectSubCategory, closeCategoriesModal } from '../../../redux/categories/actions';
import { updateCategory } from '../../../redux/operations/actions';

const mapStateToProps = state => ({
  categories: state.categories.categories,
  isCategoriesModalVisible: state.categories.isCategoriesModalVisible,
  isCollapsed: state.categories.isCollapsed,
  selectedCategory: state.categories.selectedCategory,
  selectedSubCategory: state.categories.selectedSubCategory,
  selectedOperation: state.operations.selectedOperation,
});

const mapDispatchToProps = {
  selectSubCategory,
  closeCategoriesModal,
  updateCategory,
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesModal);
