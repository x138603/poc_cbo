import { connect } from 'react-redux';
import { toggleSubCategories } from '../../redux/categories/actions';
import Categories from './Categories.component';

const mapStateToProps = state => ({
  categories: state.categories.categories,
  gdbDisabled: state.categories.gdbDisabled,
});

const mapDispatchToProps = {
  toggleSubCategories,
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
