import { connect } from 'react-redux';
import { selectSubCategory } from '../../../redux/categories/actions';
import Category from './Category.component';

const mapStateToProps = state => ({
  isCollapsed: state.categories.isCollapsed,
  selectedCategory: state.categories.selectedCategory,
  selectedSubCategory: state.categories.selectedSubCategory,
  selectedOperation: state.operations.selectedOperation,
});

const mapDispatchToProps = {
  selectSubCategory,
};

export default connect(mapStateToProps, mapDispatchToProps)(Category);
