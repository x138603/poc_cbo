import React, { Component } from 'react';
import {
  View, Text, Image, Picker,
} from 'react-native';
import Collapsible from 'react-native-collapsible';
import { getRequiredImg } from '../../../api/imageApi';

export default class Category extends Component {
  setSelectedCategory(category, selectSubCategory) {
    this.funcName = 'setSelectedCategory';
    return (itemValue) => {
      const selectedSubCategory = category.sousCategories
        .filter(item => item.id === itemValue)
        .reduce((acc, cur, i) => {
          acc[i] = cur;
          return acc;
        });
      selectSubCategory(selectedSubCategory);
    };
  }

  renderPickerItems(subCategories) {
    this.funcName = 'renderPickerItems';
    return subCategories.map(item => (
      <Picker.Item
        label={item.libelle}
        value={item.id}
        key={item.id}
      />
    ));
  }

  render() {
    const {
      category, selectSubCategory, selectedCategory, selectedSubCategory,
    } = this.props;
    const isCollapsed = selectedCategory && category.id !== selectedCategory.id;

    return (
      <View>

        <View>
          <Image
            style={styles.imageStyle}
            source={getRequiredImg(category.id)}
          />
          <Text style={styles.item}>{category.libelle}</Text>
        </View>
        <Collapsible collapsed={isCollapsed}>
          <Picker
            selectedValue={selectedSubCategory && selectedSubCategory.id}
            style={{ height: 50, width: 250 }}
            onValueChange={this.setSelectedCategory(category, selectSubCategory)
            }
          >
            {this.renderPickerItems(category.sousCategories)}
          </Picker>
        </Collapsible>


      </View>


    );
  }
}

const styles = {
  containerStyle: {
    borderWidth: 1,
    borderRadius: 2,
    borderRightColor: '#ddd',
    borderBottomColor: '#ddd',
    borderTopColor: '#ddd',
    borderLeftColor: 'blue',
    borderLeftWidth: 4,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: 'white',
    marginTop: 10,
    height: 72,
  },
  item: {
    padding: 2,
    fontSize: 16,
  },
  imageStyle: {
    height: 30,
    width: 30,
    margin: 8,
  },
};
