import { connect } from 'react-redux';
import { getOperationsDetail } from '../../redux/operations/actions';
import SynthesisDetails from './SynthesisDetails.component';

const mapStateToProps = state => ({
  operationsDetail: state.operations.operationsDetail,
});

const mapDispatchToProps = {
  getOperationsDetail,
};

export default connect(mapStateToProps, mapDispatchToProps)(SynthesisDetails);
