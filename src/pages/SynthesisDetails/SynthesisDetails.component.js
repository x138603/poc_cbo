import React, { Component } from 'react';
import { StyleSheet, Text, View, YellowBox } from 'react-native';
import Card from '../../components/Card';
import CardInfo from '../../components/CardInfo';
import AccountNumber from '../../components/AccountNumber';
import { TabsListOperation } from '../../navigation/routes';
import CategoriesModal from '../Categories/Modal';

// @todo: refacto 
export const ACCOUNT_TYPE = {
  COMPTE_A_VUE: 'comptecourant',
  COMPTE_EPARGNE: 'epargne',
  EPARGNE_FINANCIERE: 'assurances',
  credits: 'credits'
};

export const ACCOUNT_COLOR = {
  COMPTE_A_VUE: '#3e69c4',
  COMPTE_EPARGNE: '#36a592',
  EPARGNE_FINANCIERE: '#f5a623',
  credits: '#a03a9c'
};

export default class SynthesisDetails extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    return {
      headerTintColor: "white",
      headerStyle:{
        backgroundColor: ACCOUNT_COLOR[params.item.typePrestation],
        shadowRadius: 0,
        elevation:0,   
      }
      
    };
  };
  componentDidMount() {
    const { navigation: { state: { params: { item } } }, getOperationsDetail } = this.props;
    YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
    getOperationsDetail(item.idTechnique);
    this.props.navigation.setParams({
      color: "#3e69c4"
    })
  }

  render() {
    const { navigation: { state: { params: { item, theme } } }, operationsDetail } = this.props;

    return (
      <View style={styles.container}>
        <CardInfo
            type={ACCOUNT_TYPE[item.typePrestation]}
            number={item.numeroCompteFormate}
            name="Frédérique Demonceau"
        />
        <TabsListOperation screenProps={{ operationsDetail, item }} />
        <CategoriesModal />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    paddingBottom: 8,
  },
  tabStyle: {
    backgroundColor: "#3e69c4",
  },
});
