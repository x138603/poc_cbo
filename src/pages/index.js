//export { default as Contact } from './contact';

export { default as Login } from './Login';
export { default as SynthesisDesign2 } from './SynthesisDesign2';
export { default as SynthesisTabView } from './SynthesisTabView';
export { default as SynthesisDetails } from './SynthesisDetails';
export { default as CurrentOperations } from './CurrentOperations';
export { default as FutureOperations } from './FutureOperations';
export { default as Contact } from './Contact';