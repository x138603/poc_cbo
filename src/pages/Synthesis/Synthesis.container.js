import { connect } from 'react-redux';
import { getAccounts } from '../../redux/accounts/actions';
import Synthesis from './Synthesis.component';

const mapStateToProps = state => ({
  accountsGroupPerRole: state.accounts.accountsGroupPerRole,
  withAccountsAmount: state.accounts.withAmount,
});

const mapDispatchToProps = {
  getAccounts,
};

export default connect(mapStateToProps, mapDispatchToProps)(Synthesis);
