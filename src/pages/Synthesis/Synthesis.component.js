import React, { Component } from 'react';
import {
  SectionList, StyleSheet, Text, View, ScrollView, ActivityIndicator, YellowBox,
} from 'react-native';
import _ from 'lodash';
import Card from '../../components/Card';
import Amount from '../../components/Amount';
import appStyle from '../../appStyle';

const GROUP_ROLE = {
  TITULAIRE: 'Mes comptes',
  MANDATAIRE: 'Comptes par procuration',
};

const ACCOUNT_TYPE = {
  COMPTE_A_VUE: 'Comptes Bancaires',
  COMPTE_EPARGNE: 'Comptes d\'épargne',
  EPARGNE_FINANCIERE: 'Epargne financière',
};

const ACCOUNT_COLOR = {
  COMPTE_A_VUE: 'blue',
  COMPTE_EPARGNE: 'green',
  EPARGNE_FINANCIERE: 'gray',
};


export default class Synthesis extends Component {

  componentDidMount() {
    const { getAccounts } = this.props;
    YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
    // get accounts (2 calls without and with amount)
    getAccounts();
  }

  showDetailAccount(item, theme) {
    const { navigation } = this.props;
    navigation.navigate('SynthesisDetails', { item, theme });
  }

  renderListGroup(groupeRole) {
    const { withAccountsAmount } = this.props;
    const groupedAccount = (groupeRole.prestations && groupeRole.prestations.length > 0) ? _.groupBy(groupeRole.prestations, 'typePrestation') : [];
    const mappedGroupedAccount = Object.keys(groupedAccount).map((groupName) => {
      const soldeGroupe = _.find(groupeRole.soldesParGroupe, { groupeProduit: groupName }) || { solde: 0 };
      return {
        title: groupName,
        data: groupedAccount[groupName],
        solde: soldeGroupe.solde,
      };
    });

    return (
      <SectionList
        sections={mappedGroupedAccount}
        renderItem={({ item }) => (
          <Card
            theme={ACCOUNT_COLOR[item.typePrestation]}
            item={item}
            press={() => { this.showDetailAccount(item, ACCOUNT_COLOR[item.typePrestation]) }}
            withAccountsAmount={withAccountsAmount}
          />
        )}
        renderSectionHeader={({ section }) => (
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={styles.header1}>{ACCOUNT_TYPE[section.title]}</Text>
            <Amount style={styles.header1}>{withAccountsAmount ? section.solde : 'En cours de traitement'}</Amount>
          </View>
        )}
        ItemSeparatorComponent={() => (<View style={styles.separator} />)}
        keyExtractor={(item, index) => index}
      />
    );

  }

  render() {
    const { accountsGroupPerRole } = this.props;

    if (accountsGroupPerRole.length > 0) {
      return (
        <ScrollView style={styles.container}>
          {
            accountsGroupPerRole.map(groupeRole => (
              <View key={groupeRole.groupeRole}>
                <Text style={styles.groupLabel}>{GROUP_ROLE[groupeRole.groupeRole]}</Text>
                {this.renderListGroup(groupeRole)}
              </View>))
          }
        </ScrollView>
      );
    }
    return (
      <ActivityIndicator style={styles.loaderContainer} color={appStyle.colors.corail} size="large" />
    );
  }
}

const styles = StyleSheet.create({
  groupLabel: {
    color: 'white',
    fontSize: 16,
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 10,
    fontWeight: 'bold',
    backgroundColor: '#4285f4',
  },
  container: {
    backgroundColor: '#eee',
    padding: 10,
  },
  section: {
    backgroundColor: '#eee',
  },
  header1: {
    color: '#000',
    marginBottom: 10,
    marginTop: 15,
    fontSize: 16,
  },
  separator: {
    height: 10,
    width: '100%',
    backgroundColor: '#eee',
  },
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
