import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import { SgButton } from '../../../../components';

export default class Credit extends Component {
  render() {
    return (
        <View style={{flex: 1 ,flexDirection: 'column', alignItems: 'center', paddingTop: 100}}>
          <Image source={require('../../../../assets/img/1200.png')} />
          <Text style={{  margin: 30, color: "#838383", fontSize: 24 }}> Besoin d'un financement </Text>
          <SgButton
            title="Nos solutions"
            role="secondary"
            outline={true}
            style={{borderColor: "black"}}
            onPress={() => this.props.navigation.navigate('Solutions')}
          />
        </View>
    )
  }
}