import React, { Component } from 'react';
import { Text, View, StyleSheet, FlatList, ScrollView } from 'react-native';
import { AddAccountCard } from '../../../components/AddAccountCard';
import { CURRENT_ACCOUNT, ACCOUNT_COLOR, COMPTE_A_VUE } from '../constants';
import _ from 'lodash';
import Card from '../../../components/Card';
import { dateNow } from '../../../utils/dateUtils';
export default class AbstractAccounts extends Component {

  renderCurrentAccountList(groupedAccount) {
    const { withAccountsAmount } = this.props.screenProps;

    return (
      <FlatList
        data={groupedAccount}
        renderItem={({ item }) => (
          <Card
            theme={ACCOUNT_COLOR[item.typePrestation]}
            item={item}
            press={() => { this.showDetailAccount(item, ACCOUNT_COLOR[item.typePrestation]); }}
            withAccountsAmount={withAccountsAmount}
          />
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }

  showDetailAccount(item, theme) {
    const { rootNavigation } = this.getAccountConf();
    rootNavigation.navigate('SynthesisDetails', { item, theme });
  }

  getCurrentAccountType() {
    return null;
  }

  getAccountConf() {
    return null;
  }

  render() {
    const {amountStyle, cardConf, accountsGroupPerRole, withAccountsAmount} = this.getAccountConf();
    const groupedAccount = (accountsGroupPerRole[0].prestations && accountsGroupPerRole[0].prestations.length > 0)
      ? _.groupBy(accountsGroupPerRole[0].prestations, 'typePrestation') : [];
    const soldeGroupe = _.find(accountsGroupPerRole[0].soldesParGroupe, { groupeProduit: this.getCurrentAccountType() }) || { solde: 0 };

    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={{ flexDirection: 'column', alignItems: 'center', padding: 8 }}>
          <Text style={amountStyle}> {withAccountsAmount ? `${soldeGroupe.solde} €` : 'En cours de traitement'}</Text>
          <Text style={{ color: "#838383", fontSize: 13 }}> Solde total au {dateNow()} </Text>
        </View>
        <View style={styles.container}>
          {
            groupedAccount[this.getCurrentAccountType()] && groupedAccount[this.getCurrentAccountType()].length > 0
              ?
              <ScrollView>
                {this.renderCurrentAccountList(groupedAccount[this.getCurrentAccountType()])}
              </ScrollView>
              : null
          }
        </View>
        <View >
          <AddAccountCard {...cardConf} />
        </View>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    maxHeight: "68%",
  },
});
