import { INSURANCE, EPARGNE_FINANCIERE } from '../../constants';
import AbstractAccounts from '../AbstractAccounts';

export default class Insurance extends AbstractAccounts {

  getCurrentAccountType() {
    return EPARGNE_FINANCIERE;
  }

  getAccountConf() {
    const { accountsGroupPerRole, withAccountsAmount, rootNavigation } = this.props.screenProps;

    return {
      rootNavigation,
      accountsGroupPerRole,
      withAccountsAmount,
      amountStyle: { color: "#f5a623", fontSize: 24 },
      cardConf: {
        pictoType: INSURANCE,
        textConf: {
          title: {
            value: "La tranquilité Assurée",
            show: true
          },
          subTitle: {
            value: "",
            show: false
          }
        },
        buttonConf: {
          symbole: ">",
          onHandleClick: () => console.log("CurrentAccounts button add clicked ")
        },
      }
    };
  }
}
