
import { SAVING, COMPTE_EPARGNE } from '../../constants';
import AbstractAccounts from '../AbstractAccounts';

export default class Saving extends AbstractAccounts {

  getCurrentAccountType() {
    return COMPTE_EPARGNE;
  }

  getAccountConf() {
    const { accountsGroupPerRole, withAccountsAmount, rootNavigation } = this.props.screenProps;

    return {
      rootNavigation,
      accountsGroupPerRole,
      withAccountsAmount,
      amountStyle: { color: "#36a592", fontSize: 24 },
      cardConf: {
        pictoType: SAVING,
        textConf: {
          title: {
            value: "Epargnez en toute tranquilité",
            show: true
          },
          subTitle: {
            value: "Découvrez nos solutions",
            show: true
          }
        },
        buttonConf: {
          symbole: ">",
          onHandleClick: () => console.log("Saving button add clicked ")
        },
      }
    };
  }
}

