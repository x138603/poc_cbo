export const CURRENT_ACCOUNT = 'CURRENT_ACCOUNT';
export const SAVING = 'SAVING';
export const INSURANCE = 'INSURANCE';
export const COMPTE_A_VUE = 'COMPTE_A_VUE';
export const COMPTE_EPARGNE = 'COMPTE_EPARGNE';
export const EPARGNE_FINANCIERE = 'EPARGNE_FINANCIERE';

export const GROUP_ROLE = {
    TITULAIRE: 'Mes comptes',
    MANDATAIRE: 'Comptes par procuration',
};

export const ACCOUNT_TYPE = {
    COMPTE_A_VUE: 'Comptes Bancaires',
    COMPTE_EPARGNE: 'Comptes d\'épargne',
    EPARGNE_FINANCIERE: 'Epargne financière',
};

export const ACCOUNT_COLOR = {
    COMPTE_A_VUE: 'comptecourant',
    COMPTE_EPARGNE: 'epargne',
    EPARGNE_FINANCIERE: 'assurances',
};
