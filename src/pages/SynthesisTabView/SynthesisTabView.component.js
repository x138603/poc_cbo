
import React, { PureComponent } from 'react';
import SynthesisTabviewRoutes from '../../navigation/SynthesisTabviewRoutes';
import { StyleSheet, ActivityIndicator, YellowBox } from 'react-native';
import appStyle from '../../appStyle';

export class SynthesisTabView extends PureComponent {
  componentDidMount() {
    const { getAccounts } = this.props;
    YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
    // get accounts (2 calls without and with amount)
    getAccounts();
  }

  render() {
    const { accountsGroupPerRole, withAccountsAmount, rootNavigation } = this.props;
    if (accountsGroupPerRole.length > 0) {
      return (
        <SynthesisTabviewRoutes screenProps={{accountsGroupPerRole, withAccountsAmount, rootNavigation}}/>
      )
    }
    return (
      <ActivityIndicator style={styles.loaderContainer} color={appStyle.colors.corail} size="large" />
    );

  }
}

export default SynthesisTabView;

const styles = StyleSheet.create({
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
