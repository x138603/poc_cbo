import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { 
  StackNavigator,
  createStackNavigator,
  DrawerNavigator,
  createMaterialTopTabNavigator,
} from 'react-navigation';
import {
  Login,
  Contact,
  Synthesis,
  SynthesisDetails,
  CurrentOperations,
  FutureOperations,
  SynthesisTabView,
  SynthesisDesign2
} from '../pages';

import Amount from '../components/Amount';
import HamburgerIcon from './HamburgerIcon';

import accountsOperationsImg from '../assets/img/ic_accounts_operations.png';
import logoutImg from '../assets/img/ic_logout.png';
import settingstImg from '../assets/img/settings.png';

const AccountsIcon = () => <Image source={accountsOperationsImg} style={{ width: 25, height: 20 }} />;
const LogoutIcon = () => <Image source={logoutImg} style={{ width: 25, height: 25 }} />;
const SettingIcon = () => <Image source={settingstImg} style={{ width: 25, height: 25 }} />;

import elements  from '../pages/CharteGraphique/elements';
import CharteGraphique from '../pages/CharteGraphique';
import TypographyExample from '../pages/CharteGraphique/TypographyExample';
import ButtonExample from '../pages/CharteGraphique/ButtonExample';
import CardExample from '../pages/CharteGraphique/CardExample';
import CellulesExample from '../pages/CharteGraphique/CellulesExample';
import CardInfoExample from '../pages/CharteGraphique/CardInfoExample';

const exampleRoutes = Object.keys(elements)
        .map(id => (
          { 
            screen:elements[id],
            screenName:elements[id].name,
            name:id
          }
        ))
        .reduce((acc, c) => { 
          acc[c.name] = {
            screen: c.screen,
            navigationOptions: {
              title: c.name,
            }
          };
          return acc
        }, {});

const CharteGraphiqueStack = StackNavigator(
  {
    CharteGraphique: {
      screen: CharteGraphique,
      navigationOptions: ({navigation}) => ({
        title: 'Charte Graphique',
        drawerLabel: 'Charte Graphique',
        headerLeft: <HamburgerIcon navigationProps={ navigation }/>
      })
    },
    ...exampleRoutes,
    
  },
  {  
    initialRouteName: 'CharteGraphique'
  }
)

/*
  NavigationStack
    LoginStack
      login
      contact
      urgence
    DrawerStack
      synthesis
        synthesisDetails
          TabStask
            currentOperations
            futureOperations
      logout
*/
const LoginStack = createStackNavigator(
  {
    Login: { screen: Login },
    Contact: { screen: Contact, navigationOptions: { title: 'Contact', headerMode: 'floats' } },
  },
);

const SynthesisStack = createStackNavigator(
  {
    SynthesisTabView: {
      screen: ({navigation}) => (<SynthesisTabView rootNavigation={navigation}/>),
      navigationOptions: ({navigation}) => ({
        title: 'Mes Comptes',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,
      }),
    },
    SynthesisDetails: {
      screen: SynthesisDetails,
      navigationOptions: {
        title: 'Détails',
      },
    },
  },
);

const SynthesisDesign2Stack = createStackNavigator(
  {
    Synthesis: {
      screen: SynthesisDesign2,
      navigationOptions: ({navigation}) => ({
        title: 'Mes Comptes',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,
      }),
    },
    SynthesisDetails: {
      screen: SynthesisDetails,
      navigationOptions: {
        title: 'Détails',
      },
    },
  },
);

const DrawerStack = DrawerNavigator(
  {
    Synthesis: {
      screen: SynthesisStack,
      navigationOptions: {
        title: 'Synthèse Design 1',
        drawerIcon: <AccountsIcon />,
      },
    },
    SynthesisV2: {
      screen: SynthesisDesign2Stack,
      navigationOptions: {
        title: 'Synthèse Design 2',
        drawerIcon: <AccountsIcon />,
      },
    },
    CharteGraphique: {
      screen: CharteGraphiqueStack,
      navigationOptions: {
        title: 'Charte Graphique',
        drawerIcon: <SettingIcon />,
      },
    },
    Logout: {
      screen: LoginStack,
      navigationOptions: {
        title: 'Déconnexion',
        drawerIcon: <LogoutIcon />,
      },
    },
  },
  {
    initialRouteName: 'Synthesis',
    contentOptions: {
      activeTintColor: '#f05b6f',
      itemsContainerStyle: {
        margin: 0,
        padding: 0,
      },
      itemStyle: {
        height: 50,
      },
      iconContainerStyle: {
        opacity: 1,
        width: 25,
        height: 25,
      },
    },
  },
);

const AppNavigator = createStackNavigator(
  {
    loginStack: { screen: LoginStack },
    drawerStack: { screen: DrawerStack },
  },
  {
    headerMode: 'none',
    title: 'Main',
    initialRouteName: 'loginStack',
  },
);


export const TabsListOperation = createMaterialTopTabNavigator(
  {
    CurrentOperations: {
      screen: CurrentOperations,
      navigationOptions: ({ navigation }) => ({
        tabBarLabel: () => (
          <View>
            <Amount style={styles.soldeStyle}>
              {navigation.getScreenProps().operationsDetail.recapitulatifCompte.soldeTotal}
            </Amount>
            <Text style={styles.textStyle}>Solde actuel</Text>
          </View>
        ),
      }),
    },
    FutureOperations: {
      screen: FutureOperations,
      navigationOptions: ({ navigation }) => ({
        tabBarLabel: () => (
          <View>
            <Amount style={styles.soldeStyle}>
              {navigation.getScreenProps().operationsDetail.recapitulatifCompte.soldeFuture}
            </Amount>
            <Text style={styles.textStyle}>À venir</Text>
          </View>
        ),
      }),
    },
  },
  {
    tabBarOptions: {
      style: {
        backgroundColor: '#f4f4f4',
        elevation:0,
      },
      indicatorStyle: {
        backgroundColor: 'black',
      },
    },
    animationEnabled: false,
    swipeEnabled: false,
  },
);

const styles = StyleSheet.create({
  soldeStyle: {
    textAlign: 'center',
    fontWeight: 'bold',
  },
  textStyle: {
    textAlign: 'center',
  },
});

// export default App;
export default AppNavigator;
