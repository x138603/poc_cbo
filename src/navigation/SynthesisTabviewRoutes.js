import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation';
import CurrentAccounts from '../pages/SynthesisTabView/Components/CurrentAccounts/CurrentAccounts';
import Saving from '../pages/SynthesisTabView/Components/Saving/Saving';
import Insurance from '../pages/SynthesisTabView/Components/Insurance/Insurance';
import Credit from '../pages/SynthesisTabView/Components/Credit/Credit';

const styles = StyleSheet.create({
  soldeStyle: {
    textAlign: 'center',
    fontWeight: 'bold',
  },
  textStyle: {
    textAlign: 'center',
    color: 'white',
  },
  textDisabledStyle: {
    textAlign: 'center',
    color: 'black',
  },
  buttonTabCommonStyle: {
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 16,
    paddingRight: 16,
    borderRadius: 80,
    borderWidth: 1,
  },
  currentAccTabStyles: {
    borderColor: 'transparent',
    backgroundColor: '#3e69c4',
  },
  savingAccTabStyles: {
    borderColor: 'transparent',
    backgroundColor: '#36a592',
  },
  insuranceAccTabStyles: {
    borderColor: 'transparent',
    backgroundColor: '#f5a623',
  },
  creditAccTabStyles: {
    borderColor: 'transparent',
    backgroundColor: '#a03a9c',
  },
  disabledTabStyles: {
    borderColor: '#ebebeb',
    backgroundColor: '#ffffff',
  },
});

const setNavigationOptions = (title, tabBarStyles, customStyle) => ({
  tabBarLabel: (currentTab) => {
    const tabStyle = currentTab.focused ? tabBarStyles : styles.disabledTabStyles;
    const textStyle = currentTab.focused ? styles.textStyle : styles.textDisabledStyle;
    return (
      <View style={[styles.buttonTabCommonStyle, tabStyle, customStyle]}>
        <Text style={textStyle}>{title}</Text>
      </View>
    );
  },

});

export default createMaterialTopTabNavigator(
  {
    CurrentAccounts: {
      screen: ({ screenProps }) => (<CurrentAccounts screenProps={screenProps} />),
      navigationOptions: () => (setNavigationOptions("Comptes courants", styles.currentAccTabStyles)),
    },
    SavingAccounts: {
      screen: ({ screenProps }) => (<Saving screenProps={screenProps} />),
      navigationOptions: () => (setNavigationOptions("Épargne", styles.savingAccTabStyles)),
    },
    InsuranceAccounts: {
      screen: ({ screenProps }) => (<Insurance screenProps={screenProps} />),
      navigationOptions: () => (setNavigationOptions("Assurance vie", styles.insuranceAccTabStyles)),
    },
    CreditAccounts: {
      screen: Credit,
      navigationOptions: () => (setNavigationOptions("Crédit", styles.creditAccTabStyles)),
    },
  },
  {
    tabBarOptions: {
      scrollEnabled: true,
      renderIndicator: () => null,
      style: {
        backgroundColor: 'transparent',
      },
      tabStyle: {
        margin: 8,
        width: 'auto',
      }
    },
    animationEnabled: true,
    swipeEnabled: true,
  }
);
