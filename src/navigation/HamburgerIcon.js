import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import hamburgerIconImg from '../assets/img/hamburger_icon.png';


const HamburgerIcon = (props) => {
  const { navigationProps } = props;
  return (
    <View style={{ flexDirection: 'row' }}>
      <TouchableOpacity onPress={navigationProps.toggleDrawer}>
        <Image
          source={hamburgerIconImg}
          style={{ width: 25, height: 25, marginLeft: 10 }}
        />
      </TouchableOpacity>
    </View>
  );
};

export default HamburgerIcon;
