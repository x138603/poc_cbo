import React from 'react';
import { Text } from 'react-native';
import { anonymizeAccountNumber } from '../utils/bankUtils';

/* eslint-disable react/destructuring-assignment */
const AccountNumber = props => (
  <Text {...props}>{anonymizeAccountNumber(props.children)}</Text>
);

export default AccountNumber;
