import React from 'react';
import {
  StyleSheet, Text, View, Image, TouchableOpacity,
} from 'react-native';
import Amount from '../Amount';
import AccountNumber from '../AccountNumber';
import { COMPTE_A_VUE } from '../../pages/SynthesisTabView/constants';

const AccountHeader = (props) => {
  const { item, press, withAccountsAmount } = props;
  return (
    <View>
      <TouchableOpacity onPress={press} style={styles.container}>
        <View style={{ flexDirection: 'row' }}>

          <View style={{ flex: 1, justifyContent: 'space-between' }}>
            <Text style={{ color: "#333333", fontSize: 16 , fontWeight: "500"}}>{item.labelToDisplay}</Text>
            <View style={{ flexDirection: 'row', alignItems: "center" }}>
              {item.typePrestation === COMPTE_A_VUE ? <Image style={styles.image} source={require("../../assets/img/picto-sg.png")} /> : null}
              <AccountNumber>{item.numeroCompteFormate}</AccountNumber>
            </View>
          </View>

          <View style={{ flex: 1, flexDirection: "column", justifyContent: 'space-between', alignItems: 'flex-end', }}>
            <Amount style={{ color: "#333333", fontSize: 16, fontWeight: "bold" }}>
              {withAccountsAmount ? item.soldes.soldeTotal : 'En cours de traitement'}
            </Amount>
            <View style={{ alignItems: 'flex-end' }}>
              <Text>{item.typePrestation === COMPTE_A_VUE ? `A venir: ${item.soldes.soldeEnCours}€` : " "}</Text>
              <Text>{item.typePrestation === COMPTE_A_VUE ? `Prévisionnel: ${item.soldes.soldeEnCours + item.soldes.soldeTotal}€` : " "}</Text>
            </View>
          </View>

        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 12,
    paddingTop: 15,
    paddingRight: 12,
    paddingBottom: 7,
  },
  image: {
    height: 16,
    width: 16,
    marginRight: 8
  },
});

export default AccountHeader;
