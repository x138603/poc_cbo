import React, { PureComponent } from 'react';
import {
  StyleSheet, Text, View, Image, FlatList,
} from 'react-native';
import * as imgCard from '../../assets/img/8a8873323f27f1e678f8281cfe0be4ff.png';

export default class AccountFooter extends PureComponent {
  renderCard(item) {
    this.funcName = 'renderCard';
    return (
      <View style={{ flexDirection: 'row', marginBottom: 5 }}>
        <Image
          style={{ width: 24, height: 16, marginRight: 10 }}
          source={imgCard}
        />
        <Text style={{ fontSize: 12 }}>{item.labelToDisplay}</Text>
      </View>
    );
  }

  render() {
    const { item: { cartes } } = this.props;
    if (cartes.length > 0) {
      return (
        <View style={styles.container}>
          <FlatList
            data={cartes}
            renderItem={this.renderCard}
            keyExtractor={item => item.id}
          />
        </View>
      );
    }
    return null;
  }
}

const styles = StyleSheet.create({
  container: {
    borderTopWidth: 1,
    borderTopColor: '#dedede',
    paddingTop: 7,
    paddingBottom: 15,
    marginLeft: 15,
    marginRight: 15,
  },
});
