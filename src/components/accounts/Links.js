import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

/* eslint-disable-next-line no-undef */
export default AccountLinks = () => (
  <View style={styles.container}>
    <Text style={styles.link}>Virement</Text>
    <Text style={styles.link}>RIB</Text>
    <Text style={styles.link}>Relevés</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
    paddingTop: 7,
    paddingRight: 15,
    paddingBottom: 7,
  },
  link: {
    flex: 1,
    textDecorationLine: 'underline',
    textAlign: 'center',
    fontSize: 12,
  },
});
