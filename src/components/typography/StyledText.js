import * as React from 'react';
import color from 'color';
import {Text} from 'react-native';
import { withTheme } from '../../styles/theming';


class StyledText extends React.Component {
  render() {
    const { theme, alpha, family, style, ...rest } = this.props;
    const textColor = color(theme.colors.text)
      .alpha(alpha)
      .rgb()
      .string();
    const fontFamily = theme.fonts[family];

    return (
      <Text
        {...rest}
        style={[
          {color: textColor, fontFamily, textAlign: 'left'},
          style,
        ]}
      />
    );
  }
}

export default withTheme(StyledText);