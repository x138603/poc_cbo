import * as React from 'react';
import StyledText from './StyledText';
import { withTheme } from '../../styles/theming';


class Subhead extends React.Component {
  render() {
    const props = this.props;
    const {
      theme,
      style,
      ...attributes
    } = props;

    return (
      <StyledText
        {...props}
        style={[theme.typography['subhead'], props.style]}
      />
    );
  }
}
export default withTheme(Subhead);