import * as React from 'react';
import StyledText from './StyledText';
import { withTheme } from '../../styles/theming';


class Title extends React.Component {
  render() {
    const props = this.props;
    const {
      theme,
      h1,
      h2,
      h3,
      ...attributes
    } = props;
    const type = h1 && 'h1' || h2 && 'h2' || h3 && 'h3' ;
    return (
      <StyledText
        {...props}
        style={[theme.typography[type], props.style]}
      />
    );
  }
}
export default withTheme(Title);