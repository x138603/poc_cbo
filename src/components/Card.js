import React from 'react';
import { StyleSheet, View } from 'react-native';
import { AccountHeader } from './accounts';
import * as colors from "../styles/defaultTheme/colors";

const Card = (props) => {
  const { theme, item, press, withAccountsAmount, customDesign } = props;
  return (
    <View style={[styles.container, {borderLeftColor: colors[theme]}, customDesign ? customDesign : {}]}>
      <AccountHeader press={press} item={item} withAccountsAmount={withAccountsAmount} />
    </View>
  );
};

Card.defaultProps = {
  withAccountsAmount: true,
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f7f7f7',
    borderRadius: 4,
    borderRightWidth: 2,
    borderRightColor: '#dedede',
    borderTopWidth: 2,
    borderTopColor: '#dedede',
    borderBottomWidth: 2,
    borderBottomColor: '#dedede',
    borderLeftWidth: 5,
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 8,
  }
});

export default Card;
