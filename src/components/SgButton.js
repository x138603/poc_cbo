import React, { Component } from 'react';
import { ActivityIndicator, View, TouchableOpacity } from 'react-native';
import { Headline } from './';
import { withTheme } from '../styles/theming';

class SgButton extends Component {
    render() {
        const {
            style,
            theme,
            onPress,
            loading,
            title,
            icon,
            outline,
            role,
            ...attributes
        } = this.props;
        const selector = outline ? `${role}Outline` : role;
        const activityIndicatorColor = outline ? theme.colors[role] : theme.colors['white'];

        return ( 
            <View>
                <TouchableOpacity
                    style={[theme.button.container, theme.button[selector]['bg'], style]}
                    onPress={ onPress }
                >   
                    {loading ? (
                    <ActivityIndicator
                        size="small" color={activityIndicatorColor}
                    />
                    ) : (<Headline style={[theme.button[selector]['label']]}>{title}</Headline>  )}
                                
                </TouchableOpacity>    
            </View>
        );
    }
}

SgButton.defaultProps = {
    outline: false
}

export default withTheme(SgButton);