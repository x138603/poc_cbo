import React from 'react';
import { Text } from 'react-native';
import { formatAmount } from '../utils/bankUtils';

const Amount = (props) => {
  const { children } = props;
  return (
    <Text {...props}>{ Number.isNaN(Number(children)) ? children : `${formatAmount(children)} €` }</Text>
  );
};

export default Amount;
