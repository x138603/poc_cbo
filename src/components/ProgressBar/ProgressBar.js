import React, { Component } from 'react';
import setColor  from 'color';
import {
    StyleSheet,
} from 'react-native';
import ProgressBarComponent from './ProgressBarComponent'
import { withTheme } from '../../styles/theming';


class ProgressBar extends React.Component {
  render() {
    const { progress, color, style, theme } = this.props;
    const tintColor = color || theme.colors.primary;
    const trackTintColor = setColor(tintColor)
      .alpha(0.38)
      .rgb()
      .string();

    return (
      <ProgressBarComponent
        styleAttr="Horizontal"
        indeterminate={false}
        progress={progress}
        progressTintColor={tintColor}
        color={tintColor}
        style={[styles.progressBarHeight, style]}
        trackTintColor={trackTintColor}
      />
    );
  }
}

const styles = StyleSheet.create({
  progressBarHeight: {
    paddingVertical: 10,
  },
});

export default withTheme(ProgressBar);