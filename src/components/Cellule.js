import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    StyleSheet,
    TouchableHighlight 
} from 'react-native';
import { Body, Subhead, Amount } from './';
import { withTheme } from '../styles/theming';
import { getIcon } from '../api/imageApi';

class Cellule extends Component {
    render() {
        const {
            amount,
            icon,
            label,
            caption,
            chevronRight,
            ...attributes
        } = this.props;
        // @TODO: check icon existance 
        const {path, size} = getIcon(icon);
        let body, subhead, chevronRightElem;
        if (label) {
            body = <Body>{label}</Body>;
        }
        if (caption) {
            subhead = <Subhead>{caption}</Subhead>;
        }

        if (chevronRight) {
            chevronRightElem = (<Image
                style={styles.icon16}
                source={getIcon('chevron_right_black').path}
            />)
        }


        return ( 
            <View style={styles.containerStyle}>
                <TouchableHighlight>
                    <Image
                        style={styles[`icon${size}`]}
                        source={path}
                    />
                </TouchableHighlight>
                <View style={{ flex: 1, flexDirection: "column",marginLeft:16 }}>
                    {body}
                    {subhead}
                </View>
                <Amount style={ getColorizedStyleBySign(amount)}>{amount}</Amount>
                {chevronRightElem}
            </View>
        );
    }
}
Cellule.defaultProps = {
    chevronRight: false,
    icon: 'cartes_black'
}
const getColorizedStyleBySign = amount => (Math.sign(amount) >= 0
  ? styles.positiveAmountStyle
  : styles.negativeAmountStyle);

export default withTheme(Cellule);

const styles = StyleSheet.create({
    containerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 8,
        paddingRight:16,
        paddingBottom: 4,
        paddingTop: 4,        
        alignItems: 'center',
        // borderWidth: 1,
        // borderColor: "#ebebeb",
        backgroundColor: 'white',
      },
      icon32: {
        height: 32,
        width: 32,
      },      
      icon16: {
        height: 16,
        width: 16,
        marginLeft: 8,
      },
      textStyle: {
        marginRight: 32,
        marginLeft: 16,
      },
      positiveAmountStyle: {
        color: '#50b133',
      },
      negativeAmountStyle: {
        color: 'black'
      },
  });