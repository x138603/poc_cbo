import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    StyleSheet,
    TouchableHighlight 
} from 'react-native';
import { Body, Subhead, Amount } from './';
import AccountNumber from './AccountNumber';
import { withTheme } from '../styles/theming';

class CardInfo extends Component {
    render() {
        const {
            theme,
            type,
            number,
            name,
            style,
            ...attributes
        } = this.props;

        const color = theme.colors[type];

        return ( 
            <View style={[styles.card, {backgroundColor: color}, style]}>
                <AccountNumber style={styles.amount}>{number}</AccountNumber>
                <Body style={styles.name}>{name}</Body>
            </View>
        );
    }
}
CardInfo.defaultProps = {
    type: 'epargne'
}
export default withTheme(CardInfo);

const styles = StyleSheet.create({
    card: {
        paddingTop:20,
        paddingBottom:20,
        flexDirection: 'row',
    },
    amount: {
        flex:1,
        color:'white',
        textAlign:'right',
        borderRightWidth:1,
        borderRightColor:'white',
        paddingRight:15,
    },
    name: {
        flex:2,
        color:'white',
        paddingLeft:15,
    }
  });
  