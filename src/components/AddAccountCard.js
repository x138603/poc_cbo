import React, { PureComponent } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import * as colors from '../assets/styles/defaultTheme/colors';
import { CURRENT_ACCOUNT, SAVING, INSURANCE } from '../pages/SynthesisTabView/constants';

export class AddAccountCard extends PureComponent {

    getRequiredImg = (idCategory) => {
        switch (idCategory) {
            case CURRENT_ACCOUNT:
                return require('../assets/img/100.png');
            case SAVING:
                return require('../assets/img/halouf.png');
            case INSURANCE:
                return require('../assets/img/bnadm.png');
            default:
                return require('../assets/img/1200.png');
        }
    };

    render() {
        const { pictoType, textConf, buttonConf, customAddAcciuntStyle } = this.props;
        const picto = pictoType ? this.getRequiredImg(pictoType) : null;
        return (
            <View style={[styles.containerStyle, customAddAcciuntStyle ? customAddAcciuntStyle : {} ]}>
                {picto ? <Image style={styles.image} source={picto} /> : null}
                <View style={styles.textContainerStyle}>
                    <Text style={styles.textStyle}> {textConf.title.value}</Text>
                    {textConf.subTitle.show ? <Text style={{ color: "#838383", fontSize: 13 }}> {textConf.subTitle.value}</Text> : null}
                </View>
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={buttonConf.onHandleClick}
                >
                    <Text style={{ color: colors.red240, fontSize: 24 }}> {buttonConf.symbole} </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        padding: 12,
        width: "94%",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderTopColor: '#ddd',
        shadowColor: '#000',
        borderRadius: 4,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        backgroundColor: '#d5d5d5',
        margin: 10,
    },
    image: {
        height: 30,
        width: 30,
        marginRight: 8
    },
    textContainerStyle: { flex: 1, flexDirection: 'column' },
    textStyle: { color: "#737373", fontSize: 14, fontWeight: "bold" }
});

export default AddAccountCard;
