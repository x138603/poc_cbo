import React from 'react';
import {
  Text, StyleSheet, View, Image, TouchableHighlight,
} from 'react-native';
import Amount from './Amount';
import { getRequiredImg } from '../api/imageApi';

const CATEGORY_ID_OPERATION_SPLITEE = '2000';

/* eslint-disable-next-line no-undef */
export default CardOperation = (props) => {
  const {
    operation, futurOperation, onCategoryPress, selectOperation,
  } = props;
  if (operation) {
    return (
      <View style={styles.containerStyle}>
        <TouchableHighlight
          onPress={() => {
            if (operation.categorisable) {
              const isSplite = operation.cat.id === CATEGORY_ID_OPERATION_SPLITEE ? 1 : 0;
              onCategoryPress(isSplite);
              selectOperation(operation);
            }
          }}
        >
          <Image
            style={styles.imageStyle}
            source={getRequiredImg(operation.cat.id)}
          />
        </TouchableHighlight>
        <Text style={styles.textStyle}>{operation.libOpe}</Text>
        <Amount style={[styles.amountStyle, getColorizedStyleBySign(operation.mnt)]}>{operation.mnt}</Amount>
      </View>
    );
  }
  return (
    <View style={styles.containerStyle}>
      <TouchableHighlight
        onPress={() => {
          if (futurOperation.categorisable) {
            const isSplite = futurOperation.cat.id === CATEGORY_ID_OPERATION_SPLITEE ? 1 : 0;
            onCategoryPress(isSplite);
            selectOperation(futurOperation);
          }
        }}
      >
        <Image
          style={styles.imageStyle}
          source={getRequiredImg(futurOperation.cat.id)}
        />
      </TouchableHighlight>
      <Text style={styles.textStyle}>{futurOperation.libelleAAfficher}</Text>
      <Amount style={[styles.amountStyle, getColorizedStyleBySign(futurOperation.montant.value)]}>
        {futurOperation.montant.value}
      </Amount>
    </View>
  );
};

const getColorizedStyleBySign = amount => (Math.sign(amount) >= 0
  ? styles.positiveAmountStyle
  : styles.negativeAmountStyle);

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: 'white',
    marginTop: 10,
  },
  textStyle: {
    textAlign: 'left',
    flex: 1,
    padding: 5,
  },
  amountStyle: {
    maxWidth: 100,
    fontWeight: 'bold',
    padding: 5,
  },
  positiveAmountStyle: {
    color: '#50b133',
  },
  negativeAmountStyle: {
    // color: 'black'
  },
  imageStyle: {
    height: 30,
    width: 30,
    margin: 8,
  },
});
