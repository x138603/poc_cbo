import { anonymizeAccountNumber, formatAmount } from './bankUtils';

describe('bankUtils', () => {
  test('anonymizeAccountNumber with long account number should anonymized', () => {
    expect(anonymizeAccountNumber('02500 00050313049')).toBe('**** 3049');
  });

  test('anonymizeAccountNumber with 4 chars account number should anonymized', () => {
    expect(anonymizeAccountNumber('2021')).toBe('**** 2021');
  });


  test('formatAmount positive thousands amount should be formatted', () => {
    expect(formatAmount('3000')).toBe('+ 3 000,00');
  });

  test('formatAmount negative millions amount should be formatted', () => {
    expect(formatAmount('-22333444.55259')).toBe('- 22 333 444,55');
  });

  test('formatAmount small amount should be formatted', () => {
    expect(formatAmount('-0.01')).toBe('- 0,01');
  });
});
