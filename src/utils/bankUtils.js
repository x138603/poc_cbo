import numeral from 'numeral';

export const anonymizeAccountNumber = (val) => {
  const lastDigits = val.trim().substr(val.trim().length - 4);
  return lastDigits ? `**** ${lastDigits}` : '****';
};

export const formatAmount = amount => numeral(amount)
  .format('+0,0.00')
  .replace(/,/g, ' ')
  .replace('-', '- ')
  .replace('+', '+ ')
  .replace('.', ',');
