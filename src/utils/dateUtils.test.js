import { formatDateFr } from './dateUtils';

describe('dateUtils', () => {
  test('formatDateFr with integer param', () => {
    expect(formatDateFr(1535548329000)).toBe('29/08/2018');
  });

  test('formatDateFr with string param', () => {
    expect(formatDateFr('1311199200000')).toBe('21/07/2011');
  });

  test('formatDateFr with date', () => {
    expect(formatDateFr('2011-07-21')).toBe('21/07/2011');
  });
});
