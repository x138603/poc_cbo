import moment from 'moment';
import 'moment/locale/fr';

export const formatDateFr = (timestamp) => {
  const isTimestamp = /^\d{13}$/.test(timestamp);
  return isTimestamp
    ? moment(parseInt(timestamp)).format('Do/MM/YYYY')
    : moment(timestamp).format('Do/MM/YYYY');
};

export const dateNow = () => {
  return moment().format('L');
}
